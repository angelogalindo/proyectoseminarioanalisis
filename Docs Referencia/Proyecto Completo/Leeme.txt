************************************************************************************************
*											       *
*			EJEMPLO DE DESARROLLO SOFTWARE UTILIZANDO LA			       *
*					METODOLOG�A RUP					       *
*											       *
************************************************************************************************

Directorios:

\Modelo completo de Rational Rose 

	Aqu� se encuentra el modelo del proyecto dise�ado con la herramienta de modelado UML 
	"Rational Rose". El nombre del fichero con la �ltima versi�n es: "Modelo Empresa 
	Deportes", esta versi�n se corresponde con la versi�n 7.6.0109.2314 de Rational Rose
	Tambi�n est� disponible el modelo en las versiones:
		Versi�n 7.0/7.1 --- Archivo: "Deportes7_0-7_1.mdl"
		Versi�n 6.1/6.5 --- Archivo: "Deportes6_1-6_5.mdl"
		Versi�n 4.5/6.0 --- Archivo: "Deportes4_5-6_0.mdl"
		Versi�n 4.0 --- Archivo: "Deportes4_0.mdl"
		Versi�n 3.0 --- Archivo: "Deportes3_0.mdl"



\Modelo completo de Rational Rose\Deportes LSI 03 

	Aqu� se encuentra la gesti�n de requisitos del proyecto mediante la herramienta 
	"Rational Requisite Pro" 
	Versi�n de Requisite Pro: v2001a
	Rose Model Integrator ---- Version 2.2




\Aplicacion Almacen 

	Aqu� se encuentran el archivo ejecutable y el c�digo fuente de la aplicaci�n de la 
	gesti�n de almacenes. La base de datos Oracle no se encontrar� disponible.
	Los usuarios habilitados en la Base de Datos Access son los siguientes:

		Usuario 		Contrase�a
	      -------------------------------------
		  toni  		   puma         (T�cnico de Almac�n)
	


\Aplicacion Ventas 

	Aqu� se encuentran el archivo ejecutable y el c�digo fuente de la aplicaci�n de la 
	gesti�n de ventas. La base de datos Oracle no se encontrar� disponible.
	Los usuarios habilitados en la Base de Datos Access son los siguientes:

		Usuario 		Contrase�a
	      -------------------------------------
		 maria			   nike	         (Operadora)
		 pepe			   adidas	 (Operador)
		 luis			   reebok	 (Representante de Ventas)
		