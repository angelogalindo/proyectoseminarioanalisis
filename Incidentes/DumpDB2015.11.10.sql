-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.17 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para incidentes
CREATE DATABASE IF NOT EXISTS `incidentes` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `incidentes`;


-- Volcando estructura para tabla incidentes.bitacora
CREATE TABLE IF NOT EXISTS `bitacora` (
  `idbitacora` int(11) NOT NULL AUTO_INCREMENT,
  `idopcion` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idregistro` varchar(5) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechayhora` datetime DEFAULT NULL,
  `Accion` enum('Agrego','Modifico','Elimino') COLLATE utf8_spanish_ci DEFAULT NULL,
  `Cambios` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idbitacora`),
  KEY `fk_fk_opciones_bitacora` (`idopcion`),
  KEY `fk_fk_usuarios_bitacora` (`idusuario`),
  CONSTRAINT `fk_fk_opciones_bitacora` FOREIGN KEY (`idopcion`) REFERENCES `opciones` (`idopcion`),
  CONSTRAINT `fk_fk_usuarios_bitacora` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.bitacora: ~4 rows (aproximadamente)
DELETE FROM `bitacora`;
/*!40000 ALTER TABLE `bitacora` DISABLE KEYS */;
INSERT INTO `bitacora` (`idbitacora`, `idopcion`, `idusuario`, `idregistro`, `fechayhora`, `Accion`, `Cambios`) VALUES
	(2, 11, 0, '1', '2015-11-10 17:21:13', 'Agrego', NULL),
	(4, 11, 0, '2', '2015-11-10 17:24:02', 'Agrego', NULL),
	(5, 11, 0, '2', '2015-11-10 17:24:25', 'Agrego', NULL),
	(6, 11, 0, '2', '2015-11-10 17:24:54', 'Agrego', NULL),
	(7, 11, 0, '1', '2015-11-10 19:20:15', 'Modifico', 'idrol;rol;activorol;,'),
	(8, 11, 0, '1', '2015-11-10 19:21:51', 'Modifico', 'idrol;rol;activorol;,'),
	(9, 11, 0, '1', '2015-11-10 19:23:21', 'Agrego', NULL),
	(10, 11, 0, '1', '2015-11-10 19:23:25', 'Agrego', NULL);
/*!40000 ALTER TABLE `bitacora` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `idcategoria` int(11) NOT NULL,
  `categoria` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activocategoria` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.categorias: ~0 rows (aproximadamente)
DELETE FROM `categorias`;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.comentariosincidente
CREATE TABLE IF NOT EXISTS `comentariosincidente` (
  `idcomentario` int(11) NOT NULL,
  `idincidente` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `comentario` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechahoracomentario` datetime DEFAULT NULL,
  PRIMARY KEY (`idcomentario`),
  KEY `fk_fk_incidentes_comentariosincidentes` (`idincidente`),
  KEY `fk_fk_usuarios_comentariosincidente` (`idusuario`),
  CONSTRAINT `fk_fk_usuarios_comentariosincidente` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`),
  CONSTRAINT `fk_fk_incidentes_comentariosincidentes` FOREIGN KEY (`idincidente`) REFERENCES `incidentes` (`idincidente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.comentariosincidente: ~0 rows (aproximadamente)
DELETE FROM `comentariosincidente`;
/*!40000 ALTER TABLE `comentariosincidente` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentariosincidente` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.departamentos
CREATE TABLE IF NOT EXISTS `departamentos` (
  `iddepartamento` int(11) NOT NULL,
  `departamento` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`iddepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.departamentos: ~0 rows (aproximadamente)
DELETE FROM `departamentos`;
/*!40000 ALTER TABLE `departamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `departamentos` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.estados
CREATE TABLE IF NOT EXISTS `estados` (
  `idestado` int(11) NOT NULL,
  `estado` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activoestado` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.estados: ~0 rows (aproximadamente)
DELETE FROM `estados`;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.incidentes
CREATE TABLE IF NOT EXISTS `incidentes` (
  `idincidente` int(11) NOT NULL,
  `idsubcategoria` int(11) NOT NULL,
  `idestado` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idsolicitante` int(11) NOT NULL,
  `idprioridad` int(11) NOT NULL,
  `asunto` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechahoraincidente` datetime DEFAULT NULL,
  `fechahoracierre` datetime DEFAULT NULL,
  PRIMARY KEY (`idincidente`),
  KEY `fk_fk_estados_incidentes` (`idestado`),
  KEY `fk_fk_prioridad_incidentes` (`idprioridad`),
  KEY `fk_fk_solicitante_incidentes` (`idsolicitante`),
  KEY `fk_fk_subcategorias_incidentes` (`idsubcategoria`),
  KEY `fk_fk_usuarios_incidentes` (`idusuario`),
  CONSTRAINT `fk_fk_usuarios_incidentes` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`),
  CONSTRAINT `fk_fk_estados_incidentes` FOREIGN KEY (`idestado`) REFERENCES `estados` (`idestado`),
  CONSTRAINT `fk_fk_prioridad_incidentes` FOREIGN KEY (`idprioridad`) REFERENCES `prioridades` (`idprioridad`),
  CONSTRAINT `fk_fk_solicitante_incidentes` FOREIGN KEY (`idsolicitante`) REFERENCES `solicitantes` (`idsolicitante`),
  CONSTRAINT `fk_fk_subcategorias_incidentes` FOREIGN KEY (`idsubcategoria`) REFERENCES `subcategorias` (`idsubcategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.incidentes: ~0 rows (aproximadamente)
DELETE FROM `incidentes`;
/*!40000 ALTER TABLE `incidentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `incidentes` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.municipios
CREATE TABLE IF NOT EXISTS `municipios` (
  `idmunicipio` int(11) NOT NULL,
  `iddepartamento` int(11) NOT NULL,
  `municipio` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idmunicipio`),
  KEY `fk_fk_departamento_municipios` (`iddepartamento`),
  CONSTRAINT `fk_fk_departamento_municipios` FOREIGN KEY (`iddepartamento`) REFERENCES `departamentos` (`iddepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.municipios: ~0 rows (aproximadamente)
DELETE FROM `municipios`;
/*!40000 ALTER TABLE `municipios` DISABLE KEYS */;
/*!40000 ALTER TABLE `municipios` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.opciones
CREATE TABLE IF NOT EXISTS `opciones` (
  `idopcion` int(11) NOT NULL,
  `opcion` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `formularioopcion` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `enmenu` enum('Si','No') COLLATE utf8_spanish_ci DEFAULT NULL,
  `grupo` enum('Operaciones','Catalogos') COLLATE utf8_spanish_ci DEFAULT NULL,
  `activoopcion` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idopcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.opciones: ~11 rows (aproximadamente)
DELETE FROM `opciones`;
/*!40000 ALTER TABLE `opciones` DISABLE KEYS */;
INSERT INTO `opciones` (`idopcion`, `opcion`, `formularioopcion`, `enmenu`, `grupo`, `activoopcion`) VALUES
	(1, 'Listar Incidentes', 'ListadoIncidentes.aspx', 'Si', 'Operaciones', 'Activo'),
	(2, 'Crear Incidente', 'CrearIncidentes.aspx', 'No', 'Operaciones', 'Activo'),
	(3, 'Comentar Incidentes', 'ComentarIncidentes.aspx', 'No', 'Operaciones', 'Activo'),
	(4, 'Estados', 'Estados.aspx', 'Si', 'Catalogos', 'Activo'),
	(5, 'Categorias', 'Categorias.aspx', 'Si', 'Catalogos', 'Activo'),
	(6, 'SubCategorias', 'SubCategorias.aspx', 'Si', 'Catalogos', 'Activo'),
	(7, 'Prioridades', 'Prioridades.aspx', 'Si', 'Catalogos', 'Activo'),
	(8, 'Departamentos', 'Departamentos.aspx', 'Si', 'Catalogos', 'Activo'),
	(9, 'Municipios', 'Municipios.aspx', 'Si', 'Catalogos', 'Activo'),
	(10, 'Usuarios', 'Usuarios.aspx', 'Si', 'Catalogos', 'Activo'),
	(11, 'Roles de Usuario', 'RolesDeUsuario.aspx', 'Si', 'Catalogos', 'Activo');
/*!40000 ALTER TABLE `opciones` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.opcionesporrol
CREATE TABLE IF NOT EXISTS `opcionesporrol` (
  `idopcionporrol` int(11) NOT NULL AUTO_INCREMENT,
  `idrol` int(11) NOT NULL,
  `idopcion` int(11) NOT NULL,
  PRIMARY KEY (`idopcionporrol`),
  UNIQUE KEY `idrol_idopcion` (`idrol`,`idopcion`),
  KEY `fk_fk_opciones_opcionporrol` (`idopcion`),
  KEY `fk_fk_roles_opcionesporrol` (`idrol`),
  CONSTRAINT `fk_fk_opciones_opcionporrol` FOREIGN KEY (`idopcion`) REFERENCES `opciones` (`idopcion`),
  CONSTRAINT `fk_fk_roles_opcionesporrol` FOREIGN KEY (`idrol`) REFERENCES `roles` (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.opcionesporrol: ~2 rows (aproximadamente)
DELETE FROM `opcionesporrol`;
/*!40000 ALTER TABLE `opcionesporrol` DISABLE KEYS */;
INSERT INTO `opcionesporrol` (`idopcionporrol`, `idrol`, `idopcion`) VALUES
	(3, 1, 1),
	(4, 1, 2),
	(1, 2, 1),
	(2, 2, 2);
/*!40000 ALTER TABLE `opcionesporrol` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.prioridades
CREATE TABLE IF NOT EXISTS `prioridades` (
  `idprioridad` int(11) NOT NULL,
  `prioridad` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activoprioridad` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idprioridad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.prioridades: ~0 rows (aproximadamente)
DELETE FROM `prioridades`;
/*!40000 ALTER TABLE `prioridades` DISABLE KEYS */;
/*!40000 ALTER TABLE `prioridades` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activorol` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.roles: ~3 rows (aproximadamente)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`idrol`, `rol`, `activorol`) VALUES
	(0, 'Super Usuario', 'Activo'),
	(1, 'Admin Ventas', 'Activo'),
	(2, 'Usuario Normal', 'Activo');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.solicitantes
CREATE TABLE IF NOT EXISTS `solicitantes` (
  `idsolicitante` int(11) NOT NULL,
  `idmunicipio` int(11) NOT NULL,
  `solicitante` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefonosolicitante` varchar(8) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccionsolicitante` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activosolicitante` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idsolicitante`),
  KEY `fk_fk_municipios_incidentes` (`idmunicipio`),
  CONSTRAINT `fk_fk_municipios_incidentes` FOREIGN KEY (`idmunicipio`) REFERENCES `municipios` (`idmunicipio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.solicitantes: ~0 rows (aproximadamente)
DELETE FROM `solicitantes`;
/*!40000 ALTER TABLE `solicitantes` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitantes` ENABLE KEYS */;


-- Volcando estructura para procedimiento incidentes.SP_ValidaUsuario
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ValidaUsuario`(IN `Login` VARCHAR(10), IN `Contrasena` VARCHAR(20), OUT `IDUsuario` INT, OUT `Usuario` VARCHAR(50), OUT `Mensaje` VARCHAR(50))
    SQL SECURITY INVOKER
    COMMENT 'Verifica que el usuario y contraseña ingresada sean los correctos'
BEGIN
	
	Select 
		u.idusuario, concat( u.nombre, ' ' , u.apellido ) into IDUsuario,Usuario
	from usuarios u
	where u.login=Login and u.contrasena=SHA1(Contrasena);
	
	if IDUsuario IS null then
		SET Mensaje = 'Usuario y Contraseña incorrectos';
	end if;
		
END//
DELIMITER ;


-- Volcando estructura para tabla incidentes.subcategorias
CREATE TABLE IF NOT EXISTS `subcategorias` (
  `idsubcategoria` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `subcategoria` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activosubcategoria` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idsubcategoria`),
  KEY `fk_fk_categorias_subcategorias` (`idcategoria`),
  CONSTRAINT `fk_fk_categorias_subcategorias` FOREIGN KEY (`idcategoria`) REFERENCES `categorias` (`idcategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.subcategorias: ~0 rows (aproximadamente)
DELETE FROM `subcategorias`;
/*!40000 ALTER TABLE `subcategorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `subcategorias` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuario` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `login` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `contrasena` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `vencimientocontrasena` date DEFAULT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activousuario` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_fk_roles_usuario` (`idrol`),
  CONSTRAINT `fk_fk_roles_usuario` FOREIGN KEY (`idrol`) REFERENCES `roles` (`idrol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.usuarios: ~0 rows (aproximadamente)
DELETE FROM `usuarios`;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`idusuario`, `idrol`, `login`, `contrasena`, `vencimientocontrasena`, `nombre`, `apellido`, `activousuario`) VALUES
	(0, 0, 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2030-12-31', 'Super', 'Usuario', 'Activo');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;


-- Volcando estructura para vista incidentes.view_opcionesporrol
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_opcionesporrol` (
	`idopcionporrol` INT(11) NOT NULL,
	`idrol` INT(11) NOT NULL,
	`opcion` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci',
	`grupo` ENUM('Operaciones','Catalogos') NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_opcionesporrol
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_opcionesporrol`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_opcionesporrol` AS SELECT 
	opcionesporrol.idopcionporrol,
	roles.idrol,
	opciones.opcion,
	opciones.grupo
FROM opciones
INNER JOIN opcionesporrol ON opciones.idopcion=opcionesporrol.idopcion
INNER JOIN roles ON opcionesporrol.idrol=roles.idrol ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
