﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Incidentes.Operaciones
{
    public partial class ListadoIncidentes : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cmdAgregarIncidente_Click(object sender, EventArgs e)
        {
            Session["enumEstados"] = Convert.ToString((Int16)enumEstados.Nuevo);
            Response.Redirect("Incidentes.aspx");
        }

        protected void grdListadoIncidentes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["idincidente"] = Convert.ToString(grdListadoIncidentes.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoIncidentes.SelectedIndexes[0])]["idincidente"]);
            Session["idestadoIncidente"] = Convert.ToString(grdListadoIncidentes.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoIncidentes.SelectedIndexes[0])]["idestado"]);
            Session["enumEstados"] = Convert.ToString((Int16)enumEstados.Editando);
            Response.Redirect("Incidentes.aspx");
        }
    }
}