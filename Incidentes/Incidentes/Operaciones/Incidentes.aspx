﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Incidentes.aspx.cs" Inherits="Incidentes.Operaciones.Incidentes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Incidentes</h2>
    <br />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

        
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    
    <div class="row">
        <div class="col-lg-12">
            <fieldset class="form-group">     
                <legend>Ingreso y Edición</legend>
                <div class="row">
                    <div class="col-lg-4">
                        <label>Numero</label>
                        <asp:TextBox ID="txtIDIncidente" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>                    
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-8">
                        <label>Asunto</label>
                        <asp:TextBox ID="txtAsunto" CssClass="form-control" MaxLength="30"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Solicitante</label><br />
                        <telerik:RadComboBox ID="cboSolicitantes" Filter="Contains" Style="width: 60%; min-width: 150px" DataTextField="solicitante" DataValueField="idsolicitante" runat="server" DataSourceID="sqlSolicitantes"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="sqlSolicitantes" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 AS idsolicitante, 'Seleccione solicitante' AS solicitante  UNION SELECT idsolicitante, solicitante FROM solicitantes WHERE activosolicitante=1"></asp:SqlDataSource>
                    </div>                   
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <label>Descripcion</label>
                        <asp:TextBox ID="txtDescripcion" CssClass="form-control" MaxLength="30" TextMode="MultiLine"  runat="server"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-3">
                        <label>Categoria</label><br />
                        <telerik:RadComboBox ID="cboCategorias" Filter="Contains" Style="width: 100%; min-width: 150px" AutoPostBack="true" DataTextField="categoria" DataValueField="idcategoria" runat="server" DataSourceID="sqlCategorias"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="sqlCategorias" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 AS idcategoria, 'Seleccione categoria' AS categoria  UNION SELECT idcategoria, categoria FROM categorias WHERE activocategoria=1"></asp:SqlDataSource>
                    </div>
                    <div class="col-lg-3">
                        <label>SubCategoria</label><br />
                        <telerik:RadComboBox ID="cboSubcategorias" Filter="Contains" Style="width: 100%; min-width: 150px" DataTextField="subcategoria" DataValueField="idsubcategoria" runat="server" DataSourceID="sqlSubCategorias"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="sqlSubCategorias" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 AS idsubcategoria, 'Seleccione subcategoria' AS subcategoria  UNION SELECT idsubcategoria, subcategoria FROM subcategorias WHERE activosubcategoria=1 and idcategoria=@idcategoria">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="cboCategorias" Name="idcategoria" PropertyName="SelectedValue" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                     <div class="col-lg-3">
                        <label>Prioridad</label><br />
                        <telerik:RadComboBox ID="cboPrioridades" Filter="Contains" Style="width: 100%; min-width: 150px" DataTextField="prioridad" DataValueField="idprioridad" runat="server" DataSourceID="SqlPrioridades"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="SqlPrioridades" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 AS idprioridad, 'Seleccione prioridad' AS prioridad  UNION SELECT idprioridad, prioridad FROM prioridades WHERE activoprioridad=1"></asp:SqlDataSource>
                    </div>
                    <div class="col-lg-3">
                        <label>Estado</label><br />
                        <telerik:RadComboBox ID="cboEstados" Filter="Contains" Style="width: 100%; min-width: 150px" DataTextField="estado" DataValueField="idestado" runat="server" DataSourceID="SqlEstados"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="SqlEstados" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 AS idestado, 'Seleccione estado' AS estado  UNION SELECT idestado, estado FROM estados WHERE activoestado=1"></asp:SqlDataSource>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-3">
                        <label>Fecha y Hora Creacion</label>
                        <asp:TextBox ID="txtFechayHoraCreacion" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <label>Fecha y Hora Cierre</label>
                        <asp:TextBox ID="txtFechayHoraCierre" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">                        
                        <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                            <Icon PrimaryIconCssClass="rbSave"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                                <Icon PrimaryIconCssClass="rbRemove"></Icon>
                        </telerik:RadButton>
                    </div> 
                </div>
                        
            </fieldset>
        </div>
    </div>

    <asp:Panel ID="pnlComentarios" runat="server">        
        <div class="row">
            <div class="col-lg-12">
                 <fieldset class="form-group">
                    <legend>Comentarios</legend>
                     
                    <div style="border:1px solid gray; overflow-y:auto; overflow-x:hidden; max-height:300px;">
                             
                        <telerik:RadListView ID="ltvComentarios" runat="server" DataKeyNames="idcomentario" DataSourceID="sqlComentarios">
                            <ItemTemplate>
                                <div class="row">
                                <fieldset class="form-group" >
                                        <div class="col-lg-2" style="height:100%; border-right:1px solid gray">
                                            <label>
                                                <%# Eval("usuario") %>
                                            </label>
                                            <br />
                                            <label>
                                                <%# Eval("fechahora") %>
                                            </label>
                                        </div>
                                        <div class="col-lg-10">
                                            <label>
                                                <%# Eval("comentario") %> 
                                            </label>
                                        </div>
                                </fieldset>
                            </div>
                            <hr />
                            </ItemTemplate>
                        </telerik:RadListView>

                        <asp:SqlDataSource runat="server" ID="sqlComentarios" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idcomentario, idincidente, comentario, fechahora, usuario FROM view_comentarios WHERE idincidente=@idincidente ORDER BY idcomentario DESC">
                            <SelectParameters>
                                <asp:ControlParameter Name="idincidente" ControlID="txtIDIncidente" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                     <br />
                     <br />
                      
                     <telerik:RadButton ID="cmdGuardarComentarios" runat="server" Text="Guardar Comentarios" OnClick="cmdGuardarComentarios_Click"></telerik:RadButton>
                    <telerik:RadEditor ID="editComentarios"  Width="93%"  runat="server" Height="200px">
                        <Tools>           
                            <telerik:EditorToolGroup Tag="Formatting">
                                <telerik:EditorTool Name="Bold"></telerik:EditorTool>
                                <telerik:EditorTool Name="Italic"></telerik:EditorTool>
                                <telerik:EditorTool Name="Underline"></telerik:EditorTool>
                                <telerik:EditorSeparator></telerik:EditorSeparator>
                                <telerik:EditorTool Name="ForeColor"></telerik:EditorTool>
                                <telerik:EditorTool Name="BackColor"></telerik:EditorTool>
                                <telerik:EditorSeparator></telerik:EditorSeparator>
                                <telerik:EditorTool Name="FontName"></telerik:EditorTool>
                                <telerik:EditorTool Name="RealFontSize"></telerik:EditorTool>
                            </telerik:EditorToolGroup>
                        </Tools>
                    </telerik:RadEditor>
                    
                    <br />

                </fieldset>
            </div>
        </div>
        
    </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
