﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;

namespace Incidentes.Operaciones
{
    public partial class Incidentes : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };


        const int IDOpcionCrear = 2;
        const int IDOpcionComentar = 3;

        enumEstados Estado;
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        private void EventoError(string DescripcionError)
        {
            lblMensajes.Text = DescripcionError;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            cls.onError += EventoError;
            lblMensajes.Text = "";

            if (!IsPostBack)
            {
                if (Session["enumEstados"] != null)
                {
                    if ((enumEstados)Convert.ToInt16(Session["enumEstados"]) == enumEstados.Editando)
                    {
                        if (Session["idincidente"] != null)
                        {
                            CargarIncidente(Convert.ToInt16(Session["idincidente"]));
                        }
                    }
                    EstadosIncidentes();
                }
                else
                {
                    Response.Redirect("ListadoIncidentes.aspx");
                }
            }

            
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    Estado = (enumEstados)Convert.ToInt16(Session["enumEstados"]);

                    Int16 IDPk = (Estado == enumEstados.Nuevo ? Convert.ToInt16(cls.Max("incidentes", "idincidente")) : Convert.ToInt16(txtIDIncidente.Text));
                    Parametros.Add("@idincidente", MySqlDbType.Int16).Value = IDPk;
                    Parametros.Add("@asunto", MySqlDbType.VarChar, 30).Value = txtAsunto.Text;
                    Parametros.Add("@idsolicitante", MySqlDbType.Int16).Value = cboSolicitantes.SelectedValue;
                    Parametros.Add("@descripcion", MySqlDbType.VarChar, 300).Value = txtDescripcion.Text;
                    Parametros.Add("@idsubcategoria", MySqlDbType.Int16).Value = cboSubcategorias.SelectedValue;
                    Parametros.Add("@idprioridad", MySqlDbType.Int16).Value = cboPrioridades.SelectedValue;
                    Parametros.Add("@idestado", MySqlDbType.Int16).Value = cboEstados.SelectedValue;
                    Parametros.Add("@idusuario", MySqlDbType.Int16).Value = Session["IDUsuario"];
                    if (Estado == enumEstados.Nuevo)
                    {
                        Parametros.Add("@fechahoraincidente", MySqlDbType.DateTime).Value = DateTime.Now;
                    }


                    if (Estado == enumEstados.Nuevo)
                    {
                        if (cls.IngresarDatosNuevos("incidentes", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcionCrear))
                        {
                            Session["enumEstados"] = Convert.ToString((Int16)enumEstados.Editando);
                            Estado = (enumEstados)Convert.ToInt16(Session["enumEstados"]);
                            EstadosIncidentes();
                            txtIDIncidente.Text = Convert.ToString(IDPk);
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmacion", "alert('Incidente Almacenado');", true);
                        }
                    }
                    else if(Estado == enumEstados.Editando)
                    {
                        if(cboEstados.SelectedValue == "2" ){
                            Session["enumEstados"] = Convert.ToString((Int16)enumEstados.SinEstado);
                            Parametros.Add("@fechahoracierre", MySqlDbType.DateTime).Value = DateTime.Now;
                        }
                                                
                        if (cls.ActualizarDatos("incidentes", "idincidente", Convert.ToInt16(txtIDIncidente.Text), Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcionCrear))
                        {
                            EstadosIncidentes();
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmacion", "alert('Incidente Actualizado');", true);
                        }
                    }
                    Parametros.Clear();
                    
                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {

        }

        private void EstadosIncidentes()
        {
            Estado = (enumEstados)Convert.ToInt16(Session["enumEstados"]);
            switch (Estado)
            {
                case enumEstados.SinEstado:
                    //Botones                    
                    cmdGuardar.Enabled = false;
                    cmdEliminar.Enabled = false;
                    //Controles
                    txtAsunto.Enabled = false;
                    cboSolicitantes.Enabled = false;
                    txtDescripcion.Enabled = false;
                    cboCategorias.Enabled = false;
                    cboSubcategorias.Enabled = false;
                    cboPrioridades.Enabled = false;
                    cboEstados.Enabled = false;
                    cmdGuardarComentarios.Enabled = false;
                    editComentarios.Enabled = false;
                    break;                 
                case enumEstados.Nuevo:
                    //Botones                    
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = false;                    
                    //Controles
                    txtAsunto.Enabled = true;
                    cboSolicitantes.Enabled = true;
                    txtDescripcion.Enabled = true;
                    cboCategorias.Enabled = true;
                    cboSubcategorias.Enabled = true;
                    cboPrioridades.Enabled = true;
                    cboEstados.Enabled = false;
                    pnlComentarios.Visible = false;
                    //Carga Fecha y Hora
                    txtFechayHoraCreacion.Text = DateTime.Now.ToString();
                    cboEstados.SelectedValue = "1";
                    break;
                case enumEstados.Editando:
                    //Botones                   
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = true;                   
                    //Controles
                    txtAsunto.Enabled = true;
                    cboSolicitantes.Enabled = true;
                    txtDescripcion.Enabled = true;
                    cboCategorias.Enabled = true;
                    cboSubcategorias.Enabled = true;
                    cboPrioridades.Enabled = true;
                    cboEstados.Enabled = true;
                    pnlComentarios.Visible = true;
                    

                    break;
            }
        }

        private void CargarIncidente(Int16 IDIncidente)
        {
            DataTable dtIncidentes = new DataTable();
            Parametros.Add("@idincidente", MySqlDbType.Int16).Value = IDIncidente;

            dtIncidentes = cls.RetornarDatos("view_listadoincidentes", "*", Parametros, "idincidente=@idincidente");

            if (dtIncidentes.Rows.Count == 1)
            {
                txtIDIncidente.Text = dtIncidentes.Rows[0]["idincidente"].ToString();
                txtAsunto.Text = dtIncidentes.Rows[0]["asunto"].ToString();
                cboSolicitantes.SelectedValue = dtIncidentes.Rows[0]["idsolicitante"].ToString();
                txtDescripcion.Text = dtIncidentes.Rows[0]["descripcion"].ToString();
                cboCategorias.SelectedValue = dtIncidentes.Rows[0]["idcategoria"].ToString();
                cboSubcategorias.SelectedValue = dtIncidentes.Rows[0]["idsubcategoria"].ToString();
                cboPrioridades.SelectedValue = dtIncidentes.Rows[0]["idprioridad"].ToString();
                cboEstados.SelectedValue = dtIncidentes.Rows[0]["idestado"].ToString();
                txtFechayHoraCreacion.Text = dtIncidentes.Rows[0]["fechahoraincidente"].ToString();
                txtFechayHoraCierre.Text = dtIncidentes.Rows[0]["fechahoracierre"].ToString();
                if (dtIncidentes.Rows[0]["idestado"].ToString() == "2")
                {
                    Session["enumEstados"] = Convert.ToString((Int16)enumEstados.SinEstado);
                }

                //Verifica si tiene permiso de comentar
                Parametros.Clear();
                Parametros.Add("@idopcion", MySqlDbType.Int16).Value = IDOpcionComentar;
                Parametros.Add("@idusuario", MySqlDbType.Int16).Value = Convert.ToInt16(Session["IDUsuario"]);
                if( Convert.ToString(cls.RetornarValor("view_verificarpermisos", "idopcion", Parametros)) == IDOpcionComentar.ToString()){
                    editComentarios.Enabled = true;
                    cmdGuardarComentarios.Enabled = true;

                }
                else
                {
                    editComentarios.Enabled = false;
                    cmdGuardarComentarios.Enabled = false;
                }

                             
            }
            Parametros.Clear();

        }

        protected void cmdGuardarComentarios_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    Int16 IDPk =Convert.ToInt16(cls.Max("comentariosincidente", "idcomentario"));
                    Parametros.Add("@idcomentario", MySqlDbType.Int16).Value = IDPk;
                    Parametros.Add("@idincidente", MySqlDbType.Int16).Value = txtIDIncidente.Text;
                    Parametros.Add("@idusuario", MySqlDbType.Int16).Value = Session["IDUsuario"];
                    Parametros.Add("@comentario", MySqlDbType.VarChar, 300).Value = editComentarios.Content;                   
                    Parametros.Add("@fechahoracomentario", MySqlDbType.DateTime).Value = DateTime.Now;
                    if (cls.IngresarDatosNuevos("comentariosincidente", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcionComentar))
                    {
                        ltvComentarios.DataBind();
                        editComentarios.Content = "";
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmacion", "alert('Comentario ingresado');", true);
                    }
                    
                    
                }
                catch(Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }


        }

    }
}