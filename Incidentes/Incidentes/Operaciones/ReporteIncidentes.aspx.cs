﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;

namespace Incidentes.Operaciones
{
    public partial class ReporteIncidentes : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };

                
        const int IDOpcionVerTodos = 14;

        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["IDUsuario"] != null)
                {
                    //Verifica si tiene permiso de comentar
                    Parametros.Clear();
                    Parametros.Add("@idopcion", MySqlDbType.Int16).Value = IDOpcionVerTodos;
                    Parametros.Add("@idusuario", MySqlDbType.Int16).Value = Convert.ToInt16(Session["IDUsuario"]);
                    if (Convert.ToString(cls.RetornarValor("view_verificarpermisos", "idopcion", Parametros)) == IDOpcionVerTodos.ToString())
                    {
                        cboUsuarios.SelectedValue = "-2";
                        cboUsuarios.Enabled = true;
                    }
                    else
                    {
                        cboUsuarios.SelectedValue = Session["IDUsuario"].ToString();
                        cboUsuarios.Enabled = false;

                    }
                }
                else
                {
                    Response.Redirect(ResolveClientUrl("../Login.aspx"));
                }
            }

        }

        protected void cmdPDF_Click(object sender, EventArgs e)
        {
            grdIncidentesPorUsuario.ExportSettings.Pdf.PageHeader.MiddleCell.Text = "<h2>Reporte de Incidentes por Usuario</h2>";
            grdIncidentesPorUsuario.MasterTableView.ExportToPdf();
        }
    }
}