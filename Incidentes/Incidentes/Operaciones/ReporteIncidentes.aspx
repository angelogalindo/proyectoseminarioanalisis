﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteIncidentes.aspx.cs" Inherits="Incidentes.Operaciones.ReporteIncidentes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Reporte de Incidentes Por Usuario</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>

    <div class="row">
        <div class="col-lg-8">
            <label>Usuario</label><br />
            <telerik:RadComboBox ID="cboUsuarios" AutoPostBack="true" Filter="Contains" DataTextField="nombre" DataValueField="idusuario" Style="width: 40%; min-width: 150px" runat="server" DataSourceID="sqlUsuarios"></telerik:RadComboBox>
            <asp:SqlDataSource runat="server" ID="sqlUsuarios" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT -2 AS idusuario , 'Seleccione una opcion'  AS nombre UNION ALL SELECT -1 AS idusuario , 'Todos'  AS nombre UNION ALL SELECT idusuario, CONCAT(nombre, ' ', apellido) AS nombre FROM usuarios"></asp:SqlDataSource>
        
            <br /><br />
            <telerik:RadButton ID="cmdPDF" runat="server" Text="Exportar a PDF" OnClick="cmdPDF_Click"></telerik:RadButton>
            <telerik:RadGrid ID="grdIncidentesPorUsuario" runat="server" DataSourceID="sqlIncidentesPorUsuario" AllowPaging="True" AutoGenerateColumns="False" GroupPanelPosition="Top">
                <ExportSettings IgnorePaging="true" OpenInNewWindow="true" FileName="ReporteIncidentes">
                    <Pdf PageHeight="210mm" PageWidth="297mm" DefaultFontFamily="Arial Unicode MS" PageTopMargin="30mm"
                    BorderStyle="Thin" BorderColor="#666666"></Pdf>
                </ExportSettings>

                <MasterTableView AutoGenerateColumns="false" DataSourceID="sqlIncidentesPorUsuario" DataKeyNames="idincidente"  PageSize="20">
                    <Columns>
                        <telerik:GridBoundColumn DataField="idincidente" ReadOnly="True" HeaderText="Numero" SortExpression="idincidente" UniqueName="idincidente" DataType="System.Int32" FilterControlAltText="Filter idincidente column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="asunto" HeaderText="Asunto" SortExpression="asunto" UniqueName="asunto" FilterControlAltText="Filter asunto column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="solicitante" HeaderText="Solicitante" SortExpression="solicitante" UniqueName="solicitante" FilterControlAltText="Filter solicitante column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="estado" HeaderText="Estado" SortExpression="estado" UniqueName="estado" FilterControlAltText="Filter estado column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="subcategoria" HeaderText="Subcategoria" SortExpression="subcategoria" UniqueName="subcategoria" FilterControlAltText="Filter subcategoria column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="categoria" HeaderText="Categoria" SortExpression="categoria" UniqueName="categoria" FilterControlAltText="Filter categoria column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="prioridad" HeaderText="Prioridad" SortExpression="prioridad" UniqueName="prioridad" FilterControlAltText="Filter prioridad column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Anio" HeaderText="Año" SortExpression="Anio" UniqueName="Anio" DataType="System.Int64" FilterControlAltText="Filter Anio column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Mes" HeaderText="Mes" SortExpression="Mes" UniqueName="Mes" DataType="System.Int64" FilterControlAltText="Filter Mes column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DiasTranscurridos" HeaderText="Dias" SortExpression="DiasTranscurridos" UniqueName="DiasTranscurridos" DataType="System.Int64" FilterControlAltText="Filter DiasTranscurridos column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="usuario" HeaderText="Usuario" SortExpression="usuario" UniqueName="usuario" FilterControlAltText="Filter usuario column"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource runat="server" ID="sqlIncidentesPorUsuario" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idincidente, asunto, solicitante, estado, usuario, subcategoria, categoria, prioridad, Anio, Mes, DiasTranscurridos FROM view_reporte WHERE (idusuario = @idusuario OR @idusuario = - 1) ORDER BY idincidente DESC">
                <SelectParameters>
                    <asp:ControlParameter ControlID="cboUsuarios" Name="idusuario" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>

            
        </div>
    </div>
</asp:Content>
