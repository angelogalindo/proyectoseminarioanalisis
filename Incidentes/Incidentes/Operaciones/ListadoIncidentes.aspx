﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListadoIncidentes.aspx.cs" Inherits="Incidentes.Operaciones.ListadoIncidentes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Listado de Incidentes</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>

    <div class="row">
        <div class="col-lg-8">
            <telerik:RadButton ID="cmdAgregarIncidente" runat="server" Text="Agregar Incidente" OnClick="cmdAgregarIncidente_Click"></telerik:RadButton>
            <telerik:RadGrid ID="grdListadoIncidentes" runat="server" DataSourceID="sqlListadoIncidentes" OnSelectedIndexChanged="grdListadoIncidentes_SelectedIndexChanged" AllowPaging="True">
                <MasterTableView DataKeyNames="idincidente,idestado" DataSourceID="sqlListadoIncidentes" AutoGenerateColumns="False">
                    <Columns>
                        <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/edit.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="idincidente" ReadOnly="True" HeaderText="Numero" SortExpression="idincidente" UniqueName="idincidente" DataType="System.Int32" FilterControlAltText="Filter idincidente column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="idestado" Display="false" ReadOnly="True" HeaderText="idestado" SortExpression="idestado" UniqueName="idestado" DataType="System.Int32" FilterControlAltText="Filter idestado column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="asunto" HeaderText="Asunto" SortExpression="asunto" UniqueName="asunto" FilterControlAltText="Filter asunto column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="solicitante" HeaderText="Solicitante" SortExpression="solicitante" UniqueName="solicitante" FilterControlAltText="Filter solicitante column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="fechahoraincidente" HeaderText="Fecha Ingreso" SortExpression="fechahoraincidente" UniqueName="fechahoraincidente" DataType="System.DateTime" FilterControlAltText="Filter fechahoraincidente column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="estado" HeaderText="Estado" SortExpression="estado" UniqueName="estado" FilterControlAltText="Filter estado column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="login" HeaderText="Usuario" SortExpression="login" UniqueName="login" FilterControlAltText="Filter login column"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource runat="server" ID="sqlListadoIncidentes" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idincidente,idestado , asunto, solicitante, fechahoraincidente, estado, login FROM view_listadoincidentes order by idincidente desc"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
