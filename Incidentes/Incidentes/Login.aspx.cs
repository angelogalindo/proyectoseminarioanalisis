﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAD;
using MySql.Data.MySqlClient;
using System.Data;

namespace Incidentes
{
    public partial class Login : System.Web.UI.Page
    {
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            //txtUsuario.Text = "admin";
            //txtContrasena.Text = "123456";
            if (Session["IDUsuario"] != null)
            {
                Response.Redirect(ResolveClientUrl("Inicio.aspx"));
            }
        }

        protected void cmdLogin_Click(object sender, EventArgs e)
        {

            if (txtUsuario.Text != "" && txtContrasena.Text != ""){
                Parametros.Add("@Login", MySqlDbType.VarChar, 10).Value = txtUsuario.Text;
                Parametros.Add("@Contrasena", MySqlDbType.VarChar, 20).Value = txtContrasena.Text;
                Parametros.Add("@IDUsuario", MySqlDbType.Int16).Direction = ParameterDirection.Output;
                Parametros.Add("@Usuario", MySqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                Parametros.Add("@Mensaje", MySqlDbType.VarChar, 50).Direction = ParameterDirection.Output;

                
                if (cls.EjecutarSP("SP_ValidaUsuario", ref Parametros, true) == true)
                {
                    if (Parametros["@IDUsuario"].Value != DBNull.Value ){
                        Session["IDUsuario"] = Parametros["@IDUsuario"].Value;
                        Session["Login"] = txtUsuario.Text;
                        Session["Usuario"] = Parametros["@Usuario"].Value;
                        Response.Redirect(ResolveClientUrl("Inicio.aspx"));
                    }else{
                        lblMensaje.Text = Convert.ToString(Parametros["@Mensaje"].Value);
                        txtUsuario.Text = "";
                        txtContrasena.Text = "";
                    }
                }                       

                
            }else {
                lblMensaje.Text = "Debe rellenar todos los campos";
            }

            
            
        }
    }
}