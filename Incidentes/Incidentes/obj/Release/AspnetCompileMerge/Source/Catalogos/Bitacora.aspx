﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Bitacora.aspx.cs" Inherits="Incidentes.Catalogos.Bitacora" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <telerik:RadGrid ID="grdBitacora" runat="server" DataSourceID="sqlBitacora" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True">
                <MasterTableView DataSourceID="sqlBitacora" AutoGenerateColumns="False" AllowSorting="False">
                    <Columns>
                        <telerik:GridBoundColumn DataField="idregistro" Display="false" HeaderText="idregistro" SortExpression="idregistro" UniqueName="idregistro" FilterControlAltText="Filter idregistro column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="fechayhora" HeaderText="fechayhora" SortExpression="fechayhora" UniqueName="fechayhora" DataType="System.DateTime" FilterControlAltText="Filter fechayhora column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Accion" HeaderText="Accion" SortExpression="Accion" UniqueName="Accion" FilterControlAltText="Filter Accion column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="opcion" HeaderText="opcion" SortExpression="opcion" UniqueName="opcion" FilterControlAltText="Filter opcion column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="login" HeaderText="login" SortExpression="login" UniqueName="login" FilterControlAltText="Filter login column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="nombre" HeaderText="nombre" SortExpression="nombre" UniqueName="nombre" FilterControlAltText="Filter nombre column"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource runat="server" ID="sqlBitacora" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idregistro, fechayhora, Accion, opcion, login, nombre FROM view_bitacora"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
