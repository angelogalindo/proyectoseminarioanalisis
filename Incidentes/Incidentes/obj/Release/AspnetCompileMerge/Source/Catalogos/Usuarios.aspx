﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Usuarios.aspx.cs" Inherits="Incidentes.Catalogos.Usuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Usuarios</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="hdnEstado" Value="0" runat="server" />

    <div class="row">
        <div class="col-lg-12">    
            <fieldset class="form-group">     
                <legend>Ingreso y Edicion</legend>                
                <div class="row">
                    <div class="col-lg-3">
                        <label>IdUsuario</label>
                        <asp:TextBox ID="txtIDUsuario" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <label>Login</label>
                        <asp:TextBox ID="txtLogin" CssClass="form-control" MaxLength="10"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <label>Nombre</label>
                        <asp:TextBox ID="txtNombre" CssClass="form-control" MaxLength="50"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <label>Apellido</label>
                        <asp:TextBox ID="txtApellido" CssClass="form-control" MaxLength="50"  runat="server"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-3">
                        <label>Contraseña</label>
                        <asp:TextBox ID="txtContrasena" CssClass="form-control" TextMode="Password" MaxLength="20"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <label>Fecha Fencimiento</label><br />
                        <telerik:RadDatePicker ID="dtpFechaVencimiento" runat="server"></telerik:RadDatePicker>
                    </div>
                    <div class="col-lg-3">
                        <label>Rol</label><br />
                        <telerik:RadComboBox ID="cboRoles" Filter="Contains" Style="width: 60%; min-width: 150px" DataTextField="rol" DataValueField="idrol" runat="server" DataSourceID="sqlRoles"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="sqlRoles" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idrol, rol FROM roles WHERE idrol>0 and activorol=1"></asp:SqlDataSource>
                    </div>
                    <div class="col-lg-3">
                        <label>Activo</label><br />
                        <asp:CheckBox ID="chkActivo" Text="" Checked="false" runat="server" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                            <Icon PrimaryIconCssClass="rbAdd"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                            <Icon PrimaryIconCssClass="rbSave"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                                <Icon PrimaryIconCssClass="rbRemove"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                            <Icon PrimaryIconCssClass="rbCancel"></Icon>
                        </telerik:RadButton>
                    </div> 
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <fieldset>
            <legend>Listado de Usuarios</legend>
            <telerik:RadGrid ID="grdListadoUsuarios" runat="server" DataSourceID="sqlUsuarios" GroupPanelPosition="Top"
                OnSelectedIndexChanged="grdListadoUsuarios_SelectedIndexChanged">
                <ClientSettings>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                </ClientSettings>

                <MasterTableView DataKeyNames="idusuario" DataSourceID="sqlUsuarios" AutoGenerateColumns="False">
                    <Columns>
                        <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="idusuario" ReadOnly="True" Display="false" HeaderText="idusuario" SortExpression="idusuario" UniqueName="idusuario" DataType="System.Int32" FilterControlAltText="Filter idusuario column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="login" HeaderText="Login" SortExpression="login" UniqueName="login" FilterControlAltText="Filter login column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="nombre" HeaderText="Nombre" SortExpression="nombre" UniqueName="nombre" FilterControlAltText="Filter nombre column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="apellido" HeaderText="Apellido" SortExpression="apellido" UniqueName="apellido" FilterControlAltText="Filter apellido column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="rol" HeaderText="Rol" SortExpression="rol" UniqueName="rol" FilterControlAltText="Filter rol column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="activousuario" HeaderText="Estado" SortExpression="activousuario" UniqueName="activousuario" FilterControlAltText="Filter activousuario column"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource runat="server" ID="sqlUsuarios" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idusuario, login, nombre, apellido, rol, activousuario FROM view_usuarios "></asp:SqlDataSource>
        </fieldset>
    </div> 
</asp:Content>
