﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Estados.aspx.cs" Inherits="Incidentes.Catalogos.Estados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Estados</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="hdnEstado" Value="0" runat="server" />

    <div class="row">
        <div class="col-lg-6">
            <fieldset class="form-group">     
                <legend>Ingreso y Edición</legend>
                <div class="row">
                    <div class="col-lg-4">
                        <label>IdEstado</label>
                        <asp:TextBox ID="txtIDEstado" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Estado</label>
                        <asp:TextBox ID="txtEstado" CssClass="form-control" MaxLength="15"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Activo</label><br />
                        <asp:CheckBox ID="chkActivo" Text="" Checked="false" runat="server" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                            <Icon PrimaryIconCssClass="rbAdd"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                            <Icon PrimaryIconCssClass="rbSave"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                                <Icon PrimaryIconCssClass="rbRemove"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                            <Icon PrimaryIconCssClass="rbCancel"></Icon>
                        </telerik:RadButton>
                    </div> 
                </div>
                        
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <fieldset>
                <legend>Listado de Estados</legend>
                <telerik:RadGrid ID="grdListadoEstados" runat="server" DataSourceID="sqlListadoEstados"
                    OnSelectedIndexChanged="grdListadoEstados_SelectedIndexChanged">
                    <MasterTableView DataKeyNames="idestado" DataSourceID="sqlListadoEstados" AutoGenerateColumns="False">
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="idestado" Display="false" ReadOnly="True" HeaderText="idestado" SortExpression="idestado" UniqueName="idestado" DataType="System.Int32" FilterControlAltText="Filter idestado column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="estado" HeaderText="Estado" SortExpression="estado" UniqueName="estado" FilterControlAltText="Filter estado column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="activoestado" HeaderText="Activo" SortExpression="activoestado" UniqueName="activoestado" FilterControlAltText="Filter activoestado column"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>

                <asp:SqlDataSource runat="server" ID="sqlListadoEstados" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idestado, estado, activoestado FROM estados"></asp:SqlDataSource>
            </fieldset>
        </div>
    </div>
</asp:Content>
