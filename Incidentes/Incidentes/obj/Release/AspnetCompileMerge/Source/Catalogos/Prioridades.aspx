﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Prioridades.aspx.cs" Inherits="Incidentes.Catalogos.Prioridades" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Prioridades</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="hdnEstado" Value="0" runat="server" />
    <div class="row">
        <div class="col-lg-6">
            <fieldset class="form-group">     
                <legend>Ingreso y Edición</legend>
                <div class="row">
                    <div class="col-lg-4">
                        <label>IdPrioridad</label>
                        <asp:TextBox ID="txtIDPrioridad" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Prioridad</label>
                        <asp:TextBox ID="txtPrioridad" CssClass="form-control" MaxLength="10"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Activo</label><br />
                        <asp:CheckBox ID="chkActivo" Text="" Checked="false" runat="server" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                            <Icon PrimaryIconCssClass="rbAdd"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                            <Icon PrimaryIconCssClass="rbSave"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                                <Icon PrimaryIconCssClass="rbRemove"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                            <Icon PrimaryIconCssClass="rbCancel"></Icon>
                        </telerik:RadButton>
                    </div> 
                </div>
            </fieldset>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <fieldset>
                <legend>Listado de Prioridades</legend>
                <telerik:RadGrid ID="grdListadoPrioridades" runat="server" DataSourceID="sqlListadoPrioridades" 
                    OnSelectedIndexChanged="grdListadoPrioridades_SelectedIndexChanged">
                    <MasterTableView DataKeyNames="idprioridad" DataSourceID="sqlListadoPrioridades" AutoGenerateColumns="False">
                        <Columns>
                             <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="idprioridad" Display="false" ReadOnly="True" HeaderText="idprioridad" SortExpression="idprioridad" UniqueName="idprioridad" DataType="System.Int32" FilterControlAltText="Filter idprioridad column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="prioridad" HeaderText="Prioridad" SortExpression="prioridad" UniqueName="prioridad" FilterControlAltText="Filter prioridad column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="activoprioridad" HeaderText="Estado" SortExpression="activoprioridad" UniqueName="activoprioridad" FilterControlAltText="Filter activoprioridad column"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource runat="server" ID="sqlListadoPrioridades" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idprioridad, prioridad, activoprioridad FROM prioridades"></asp:SqlDataSource>
            </fieldset>
        </div>
    </div>
</asp:Content>
