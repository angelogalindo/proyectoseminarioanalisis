﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubCategorias.aspx.cs" Inherits="Incidentes.Catalogos.SubCategorias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">SubCategorías</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="hdnEstado" Value="0" runat="server" />

    <div class="row">
        <div class="col-lg-7">
            <fieldset class="form-group">     
                <legend>Ingreso y Edición</legend>
                <div class="row">
                    <div class="col-lg-3">
                        <label>IdSubCategoría</label>
                        <asp:TextBox ID="txtIDSubCategoria" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <label>SubCategoría</label>
                        <asp:TextBox ID="txtSubCategoria" CssClass="form-control" MaxLength="20"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <label>Categoría</label><br />
                        <telerik:RadComboBox ID="cboCategorias" Filter="Contains" Style="width: 60%; min-width: 150px" DataTextField="categoria" DataValueField="idcategoria" runat="server" DataSourceID="sqlCategorias"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="sqlCategorias" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 AS idcategoria, 'Seleccione una categoria' AS categoria UNION SELECT idcategoria, categoria FROM categorias WHERE activocategoria=1"></asp:SqlDataSource>
                    </div>
                    <div class="col-lg-3">
                        <label>Activo</label><br />
                        <asp:CheckBox ID="chkActivo" Text="" Checked="false" runat="server" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                            <Icon PrimaryIconCssClass="rbAdd"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                            <Icon PrimaryIconCssClass="rbSave"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                                <Icon PrimaryIconCssClass="rbRemove"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                            <Icon PrimaryIconCssClass="rbCancel"></Icon>
                        </telerik:RadButton>
                    </div> 
                </div>
            </fieldset>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-7">
            <fieldset>
                <legend>Listado de SubCategorías</legend>
                <telerik:RadGrid ID="grdSubCategorias" runat="server" DataSourceID="sqlSubCategorias" 
                    OnSelectedIndexChanged="grdSubCategorias_SelectedIndexChanged">
                    <MasterTableView DataKeyNames="idsubcategoria" DataSourceID="sqlSubCategorias" AutoGenerateColumns="False">
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="idsubcategoria" Display="false" ReadOnly="True" HeaderText="idsubcategoria" SortExpression="idsubcategoria" UniqueName="idsubcategoria" DataType="System.Int32" FilterControlAltText="Filter idsubcategoria column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="subcategoria" HeaderText="SubCategoria" SortExpression="subcategoria" UniqueName="subcategoria" FilterControlAltText="Filter subcategoria column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="categoria" HeaderText="Categoria" SortExpression="categoria" UniqueName="categoria" FilterControlAltText="Filter categoria column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="activosubcategoria" HeaderText="Estado" SortExpression="activosubcategoria" UniqueName="activosubcategoria" FilterControlAltText="Filter activosubcategoria column"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource runat="server" ID="sqlSubCategorias" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idsubcategoria, subcategoria, categoria, activosubcategoria FROM view_subcategorias"></asp:SqlDataSource>
            </fieldset>
        </div>
    </div>
</asp:Content>
