﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Solicitantes.aspx.cs" Inherits="Incidentes.Solicitantes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Solicitantes</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="hdnEstado" Value="0" runat="server" />

    <div class="row">
        <div class="col-lg-6">
            <fieldset class="form-group">     
                <legend>Ingreso y Edición</legend>
                <div class="row">
                    <div class="col-lg-4">
                        <label>IdSolicitante</label>
                        <asp:TextBox ID="txtIDSolicitante" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Solicitante</label>
                        <asp:TextBox ID="txtSolicitante" CssClass="form-control" MaxLength="20"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Telefono</label>
                        <asp:TextBox ID="txtTelefono" CssClass="form-control" MaxLength="8"  runat="server"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-4">
                        <label>Direccion</label>
                        <asp:TextBox ID="txtDireccion" CssClass="form-control" MaxLength="20"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Municipio</label><br />
                        <telerik:RadComboBox ID="cboMunicipios" Filter="Contains" Style="width: 60%; min-width: 150px" DataTextField="municipio" DataValueField="idmunicipio" runat="server" DataSourceID="sqlMunicipios"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="sqlMunicipios" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 AS idmunicipio, 'Seleccione municipio' AS municipio  UNION SELECT idmunicipio, municipio FROM municipios"></asp:SqlDataSource>
                    </div>
                    <div class="col-lg-4">
                        <label>Activo</label><br />
                        <asp:CheckBox ID="chkActivo" Text="" Checked="false" runat="server" />
                    </div>
                </div>

                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                            <Icon PrimaryIconCssClass="rbAdd"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                            <Icon PrimaryIconCssClass="rbSave"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                                <Icon PrimaryIconCssClass="rbRemove"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                            <Icon PrimaryIconCssClass="rbCancel"></Icon>
                        </telerik:RadButton>
                    </div> 
                </div>
                        
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <fieldset>
                <legend>Listado de Solicitantes</legend>
                <telerik:RadGrid ID="grdListadoSolicitantes" runat="server" DataSourceID="sqlListadoSolicitantes" 
                    OnSelectedIndexChanged="grdListadoSolicitantes_SelectedIndexChanged">
                    <MasterTableView DataKeyNames="idsolicitante" DataSourceID="sqlListadoSolicitantes" AutoGenerateColumns="False">
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="idsolicitante" Display="false" ReadOnly="True" HeaderText="idsolicitante" SortExpression="idsolicitante" UniqueName="idsolicitante" DataType="System.Int32" FilterControlAltText="Filter idsolicitante column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="solicitante" HeaderText="Solicitante" SortExpression="solicitante" UniqueName="solicitante" FilterControlAltText="Filter solicitante column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="telefonosolicitante" HeaderText="Telefono" SortExpression="telefonosolicitante" UniqueName="telefonosolicitante" FilterControlAltText="Filter telefonosolicitante column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="direccionsolicitante" HeaderText="Direccion" SortExpression="direccionsolicitante" UniqueName="direccionsolicitante" FilterControlAltText="Filter direccionsolicitante column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="municipio" HeaderText="Municipio" SortExpression="municipio" UniqueName="municipio" FilterControlAltText="Filter municipio column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="activosolicitante" HeaderText="Estado" SortExpression="activosolicitante" UniqueName="activosolicitante" FilterControlAltText="Filter activosolicitante column"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>

                <asp:SqlDataSource runat="server" ID="sqlListadoSolicitantes" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idsolicitante, solicitante, telefonosolicitante, direccionsolicitante, municipio, activosolicitante FROM view_solicitantes"></asp:SqlDataSource>
            </fieldset>
        </div>
    </div>
</asp:Content>
