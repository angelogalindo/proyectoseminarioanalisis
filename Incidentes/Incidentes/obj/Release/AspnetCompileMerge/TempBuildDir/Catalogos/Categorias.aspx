﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Categorias.aspx.cs" Inherits="Incidentes.Catalogos.Categorias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Categorías</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="hdnEstado" Value="0" runat="server" />

    <div class="row">
        <div class="col-lg-6">
            <fieldset class="form-group">     
                <legend>Ingreso y Edición</legend>
                <div class="row">
                    <div class="col-lg-4">
                        <label>IdCategoría</label>
                        <asp:TextBox ID="txtIDCategoria" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Categoría</label>
                        <asp:TextBox ID="txtCategoria" CssClass="form-control" MaxLength="20"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Activo</label><br />
                        <asp:CheckBox ID="chkActivo" Text="" Checked="false" runat="server" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                            <Icon PrimaryIconCssClass="rbAdd"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                            <Icon PrimaryIconCssClass="rbSave"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                                <Icon PrimaryIconCssClass="rbRemove"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                            <Icon PrimaryIconCssClass="rbCancel"></Icon>
                        </telerik:RadButton>
                    </div> 
                </div>
                        
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <fieldset>
                <legend>Listado de Categorías</legend>
                <telerik:RadGrid ID="grdListadoCategorias" runat="server" DataSourceID="sqlListadoCategorias" 
                    OnSelectedIndexChanged="grdListadoCategorias_SelectedIndexChanged">
                    <MasterTableView DataKeyNames="idcategoria" DataSourceID="sqlListadoCategorias" AutoGenerateColumns="False">
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="idcategoria" ReadOnly="True" Display="false" HeaderText="idcategoria" SortExpression="idcategoria" UniqueName="idcategoria" DataType="System.Int32" FilterControlAltText="Filter idcategoria column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="categoria" HeaderText="Categoria" SortExpression="categoria" UniqueName="categoria" FilterControlAltText="Filter categoria column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="activocategoria" HeaderText="Estado" SortExpression="activocategoria" UniqueName="activocategoria" FilterControlAltText="Filter activocategoria column"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource runat="server" ID="sqlListadoCategorias" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idcategoria, categoria, activocategoria FROM categorias"></asp:SqlDataSource>
            </fieldset>
        </div>
    </div>
</asp:Content>
