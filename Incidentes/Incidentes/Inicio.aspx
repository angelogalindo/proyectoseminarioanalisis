﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="Incidentes.Inicio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-6">
            <telerik:RadHtmlChart ID="chartPorcentajes" runat="server" DataSourceID="sqlPorcentajes">
                <ChartTitle Text="Porcentajes de Incidentes por Usuario">
                </ChartTitle>
                <PlotArea>
                    <Series>
                        <telerik:PieSeries StartAngle="90" DataFieldY="Porcentaje" NameField="usuario">
                        </telerik:PieSeries>
                    </Series>
                </PlotArea>
            </telerik:RadHtmlChart>
            <asp:SqlDataSource runat="server" ID="sqlPorcentajes" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT usuario, Porcentaje FROM view_dash_porcentajesusuarios"></asp:SqlDataSource>
        </div>
        <div class="col-lg-6">
            <telerik:RadHtmlChart ID="chartCategoria" runat="server" DataSourceID="sqlPorcentajesPorSubcategoria">
                <ChartTitle Text="Porcentajes de Incidentes segun subcategorias">
                </ChartTitle>
                <PlotArea>
                    <Series>
                        <telerik:ColumnSeries DataFieldY="porcentaje"></telerik:ColumnSeries>
                    </Series>
                    <XAxis DataLabelsField="subcategoria">
                        <LabelsAppearance RotationAngle="75">
                        </LabelsAppearance>
                        <TitleAppearance Text="Subcategorias">
                        </TitleAppearance>
                    </XAxis>
                </PlotArea>
            </telerik:RadHtmlChart>
            <asp:SqlDataSource runat="server" ID="sqlPorcentajesPorSubcategoria" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT subcategoria, porcentaje FROM view_dash_subcategorias"></asp:SqlDataSource>
        </div>
    </div>
    

    
</asp:Content>
