﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;

namespace Incidentes
{
    public partial class Solicitantes : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };

        const int IDOpcion = 4;
        enumEstados Estado;
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            cls.onError += EventoError;
            lblMensajes.Text = "";
            if (!IsPostBack)
            {
                Estados();
                Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
            }
        }

        protected void cmdNuevo_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Nuevo);
            Estados();
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    byte Activo;
                    Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
                    Activo = (chkActivo.Checked ? (byte)1 : (byte)2);

                    Int16 IDPk = (Estado == enumEstados.Nuevo ? Convert.ToInt16(cls.Max("solicitantes", "idsolicitante")) : Convert.ToInt16(txtIDSolicitante.Text));

                    Parametros.Add("@idsolicitante", MySqlDbType.Int16).Value = IDPk;
                    Parametros.Add("@solicitante", MySqlDbType.VarChar, 20).Value = txtSolicitante.Text;
                    Parametros.Add("@telefonosolicitante", MySqlDbType.VarChar, 8).Value = txtTelefono.Text;
                    Parametros.Add("@direccionsolicitante", MySqlDbType.VarChar, 20).Value = txtDireccion.Text;
                    Parametros.Add("@idmunicipio", MySqlDbType.Int16).Value = cboMunicipios.SelectedValue;
                    Parametros.Add("@activosolicitante", MySqlDbType.Enum).Value = (Estado == enumEstados.Nuevo ? 1 : Activo);

                    if (Estado == enumEstados.Nuevo)
                    {
                        if (cls.IngresarDatosNuevos("solicitantes", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            txtIDSolicitante.Text = Convert.ToString(IDPk);
                            chkActivo.Checked = true;
                            grdListadoSolicitantes.DataBind();
                        }
                    }
                    else if (Estado == enumEstados.Editando)
                    {
                        if (cls.ActualizarDatos("solicitantes", "idsolicitante", Convert.ToInt16(txtIDSolicitante.Text), Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            grdListadoSolicitantes.DataBind();
                        }
                    }
                    Parametros.Clear();

                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    Int16 IDPk = Convert.ToInt16(txtIDSolicitante.Text);
                    if (cls.EliminarDatos("solicitantes", "idsolicitante", IDPk, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                    {
                        hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
                        Estados();
                        LimpiarControles();
                        grdListadoSolicitantes.Rebind();
                    }
                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
            Estados();
            LimpiarControles();
        }

        protected void grdListadoSolicitantes_SelectedIndexChanged(object sender, EventArgs e)
        {
            string IDPk = Convert.ToString(grdListadoSolicitantes.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoSolicitantes.SelectedIndexes[0])]["idsolicitante"]);
            CargarDatos(Convert.ToInt16(IDPk)); 
        }

        private void EventoError(string DescripcionError)
        {
            lblMensajes.Text = DescripcionError;
        }

        #region Funciones
        private void Estados()
        {
            Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);

            switch (Estado)
            {
                case enumEstados.SinEstado:
                    //Botones
                    cmdNuevo.Enabled = true;
                    cmdGuardar.Enabled = false;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = false;
                    //Controles                    
                    txtSolicitante.Enabled = false;
                    txtTelefono.Enabled = false;
                    txtDireccion.Enabled = false;
                    cboMunicipios.Enabled = false;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Nuevo:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtSolicitante.Enabled = true;
                    txtTelefono.Enabled = true;
                    txtDireccion.Enabled = true;
                    cboMunicipios.Enabled = true;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Editando:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = true;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtSolicitante.Enabled = true;
                    txtTelefono.Enabled = true;
                    txtDireccion.Enabled = true;
                    cboMunicipios.Enabled = true;
                    chkActivo.Enabled = true;
                    break;
            }

        }

        private void CargarDatos(Int16 IDPk)
        {
            DataTable dtRoles = new DataTable();
            Parametros.Add("@idsolicitante", MySqlDbType.Int16).Value = IDPk;


            dtRoles = cls.RetornarDatos("solicitantes", "*", Parametros, "idsolicitante=@idsolicitante");
            try
            {
                if (dtRoles.Rows.Count == 1)
                {
                    txtIDSolicitante.Text = dtRoles.Rows[0]["idsolicitante"].ToString();
                    txtSolicitante.Text = dtRoles.Rows[0]["solicitante"].ToString();
                    txtTelefono.Text = dtRoles.Rows[0]["telefonosolicitante"].ToString();
                    txtDireccion.Text = dtRoles.Rows[0]["direccionsolicitante"].ToString();
                    cboMunicipios.SelectedValue = dtRoles.Rows[0]["idmunicipio"].ToString();

                    if (dtRoles.Rows[0]["activosolicitante"].ToString() == "Activo")
                    {
                        chkActivo.Checked = true;
                    }
                    else if (dtRoles.Rows[0]["activosolicitante"].ToString() == "Inactivo")
                    {
                        chkActivo.Checked = false;
                    }
                    hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                    Estados();
                }

                dtRoles.Dispose();
                Parametros.Clear();
            }
            catch (Exception ex)
            {
                cls.LevantarError(ex);
            }

        }

        private void LimpiarControles()
        {
            txtIDSolicitante.Text = "";
            txtSolicitante.Text = "";
            txtTelefono.Text = "";
            txtDireccion.Text = "";
            cboMunicipios.SelectedValue = "0";
            chkActivo.Checked = false;

        }
        #endregion
    }
}