﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;

namespace Incidentes.Catalogos
{
    public partial class Estados : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };

        const int IDOpcion = 5;
        enumEstados Estado;
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            cls.onError += EventoError;
            lblMensajes.Text = "";
            if (!IsPostBack)
            {
                Estados1();
                Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
            }
        }

        protected void cmdNuevo_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Nuevo);
            Estados1();
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    byte Activo;
                    Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
                    Activo = (chkActivo.Checked ? (byte)1 : (byte)2);

                    Int16 IDPk = (Estado == enumEstados.Nuevo ? Convert.ToInt16(cls.Max("estados", "idestado")) : Convert.ToInt16(txtIDEstado.Text));

                    Parametros.Add("@idestado", MySqlDbType.Int16).Value = IDPk;
                    Parametros.Add("@estado", MySqlDbType.VarChar, 10).Value = txtEstado.Text;
                    Parametros.Add("@activoestado", MySqlDbType.Enum).Value = (Estado == enumEstados.Nuevo ? 1 : Activo);

                    if (Estado == enumEstados.Nuevo)
                    {
                        if (cls.IngresarDatosNuevos("estados", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {

                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados1();
                            txtIDEstado.Text = Convert.ToString(IDPk);
                            chkActivo.Checked = true;
                            grdListadoEstados.DataBind();
                        }
                    }
                    else if (Estado == enumEstados.Editando)
                    {
                        if (cls.ActualizarDatos("estados", "idestado", Convert.ToInt16(txtIDEstado.Text), Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados1();
                            grdListadoEstados.DataBind();
                        }
                    }
                    Parametros.Clear();

                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    Int16 IDPk = Convert.ToInt16(txtIDEstado.Text);
                    if (cls.EliminarDatos("estados", "idestado", IDPk, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                    {
                        hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
                        Estados1();
                        LimpiarControles();
                        grdListadoEstados.Rebind();
                    }
                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
            Estados1();
            LimpiarControles();
        }

        protected void grdListadoEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            string IDPk = Convert.ToString(grdListadoEstados.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoEstados.SelectedIndexes[0])]["idestado"]);
            CargarDatos(Convert.ToInt16(IDPk));   
        }

        private void EventoError(string DescripcionError)
        {
            lblMensajes.Text = DescripcionError;
        }

        #region Funciones
        private void Estados1()
        {
            Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);

            switch (Estado)
            {
                case enumEstados.SinEstado:
                    //Botones
                    cmdNuevo.Enabled = true;
                    cmdGuardar.Enabled = false;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = false;
                    //Controles                    
                    txtEstado.Enabled = false;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Nuevo:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtEstado.Enabled = true;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Editando:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = true;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtEstado.Enabled = true;
                    chkActivo.Enabled = true;
                    break;
            }

        }

        private void CargarDatos(Int16 IDPk)
        {
            DataTable dtRoles = new DataTable();
            Parametros.Add("@idestado", MySqlDbType.Int16).Value = IDPk;


            dtRoles = cls.RetornarDatos("estados", "*", Parametros, "idestado=@idestado");
            try
            {
                if (dtRoles.Rows.Count == 1)
                {
                    txtIDEstado.Text = dtRoles.Rows[0]["idestado"].ToString();
                    txtEstado.Text = dtRoles.Rows[0]["estado"].ToString();

                    if (dtRoles.Rows[0]["activoestado"].ToString() == "Activo")
                    {
                        chkActivo.Checked = true;
                    }
                    else if (dtRoles.Rows[0]["activoestado"].ToString() == "Inactivo")
                    {
                        chkActivo.Checked = false;
                    }
                    hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                    Estados1();
                }

                dtRoles.Dispose();
                Parametros.Clear();
            }
            catch (Exception ex)
            {
                cls.LevantarError(ex);
            }

        }

        private void LimpiarControles()
        {
            txtIDEstado.Text = "";
            txtEstado.Text = "";
            chkActivo.Checked = false;

        }
        #endregion
    }
}