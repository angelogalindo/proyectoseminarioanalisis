﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;

namespace Incidentes.Catalogos
{
    public partial class Usuarios : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };

        const int IDOpcion = 11;
        enumEstados Estado;
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            cls.onError += EventoError;
            lblMensajes.Text = "";
            if (!IsPostBack)
            {
                Estados();
                Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
            }
        }

        protected void cmdNuevo_Click(object sender, EventArgs e)
        {
            dtpFechaVencimiento.SelectedDate = DateTime.Today.AddMonths(3);
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Nuevo);
            Estados();
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    byte Activo;
                    Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
                    Activo = (chkActivo.Checked ? (byte)1 : (byte)2);

                    Int16 IDPk = (Estado == enumEstados.Nuevo ? Convert.ToInt16(cls.Max("usuarios", "idusuario")) : Convert.ToInt16(txtIDUsuario.Text));

                    Parametros.Add("@idusuario", MySqlDbType.Int16).Value = IDPk;
                    Parametros.Add("@login", MySqlDbType.VarChar, 10).Value = txtLogin.Text;
                    Parametros.Add("@vencimientocontrasena", MySqlDbType.Date).Value = dtpFechaVencimiento.SelectedDate;
                    Parametros.Add("@nombre", MySqlDbType.VarChar, 50).Value = txtNombre.Text;
                    Parametros.Add("@apellido", MySqlDbType.VarChar, 50).Value = txtApellido.Text;                    
                    Parametros.Add("@idrol", MySqlDbType.Int16).Value = cboRoles.SelectedValue;
                    Parametros.Add("@activousuario", MySqlDbType.Enum).Value = (Estado == enumEstados.Nuevo ? 1 : Activo);

                    if (Estado == enumEstados.Nuevo)
                    {
                        if (cls.IngresarDatosNuevos("usuarios", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            Parametros.Clear();
                            Parametros.Add("@idusuario", MySqlDbType.Int16).Value = IDPk;
                            Parametros.Add("@contrasena", MySqlDbType.Int16).Value = txtContrasena.Text;
                            if (cls.EjecutarSQL("UPDATE usuarios SET contrasena=SHA1(@contrasena) WHERE idusuario=@idusuario", Parametros))
                            {
                                hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                                Estados();
                                txtIDUsuario.Text = Convert.ToString(IDPk);
                                chkActivo.Checked = true;
                                grdListadoUsuarios.DataBind();
                            }                            
                        }
                    }
                    else if (Estado == enumEstados.Editando)
                    {
                        if (cls.ActualizarDatos("usuarios", "idusuario", Convert.ToInt16(txtIDUsuario.Text), Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            if (txtContrasena.Text != "")
                            {
                                Parametros.Clear();
                                Parametros.Add("@idusuario", MySqlDbType.Int16).Value = IDPk;
                                Parametros.Add("@contrasena", MySqlDbType.Int16).Value = txtContrasena.Text;
                                cls.EjecutarSQL("UPDATE usuarios SET contrasena=SHA1(@contrasena) WHERE idusuario=@idusuario", Parametros);
                            }
                          
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            grdListadoUsuarios.DataBind();
                        }

                        

                    }
                    Parametros.Clear();

                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }


            }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
            Estados();
            LimpiarControles();
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    Int16 IDPk =  Convert.ToInt16(txtIDUsuario.Text);
                    if (cls.EliminarDatos("usuarios", "idusuario", IDPk, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                    {
                        hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
                        Estados();
                        LimpiarControles();
                        grdListadoUsuarios.Rebind();
                    }
                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        private void EventoError(string DescripcionError)
        {
            lblMensajes.Text = DescripcionError;
        }

        #region Funciones
        private void Estados()
        {
            Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);

            switch (Estado)
            {
                case enumEstados.SinEstado:
                    //Botones
                    cmdNuevo.Enabled = true;
                    cmdGuardar.Enabled = false;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = false;
                    //Controles
                    txtIDUsuario.Enabled = false;
                    txtLogin.Enabled = false;
                    txtNombre.Enabled = false;
                    txtApellido.Enabled = false;
                    txtContrasena.Enabled = false;
                    dtpFechaVencimiento.Enabled = false;
                    cboRoles.Enabled = false;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Nuevo:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = true;
                    //Controles                     
                    txtLogin.Enabled = true;
                    txtNombre.Enabled = true;
                    txtApellido.Enabled = true;
                    txtContrasena.Enabled = true;
                    dtpFechaVencimiento.Enabled = false;
                    cboRoles.Enabled = true;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Editando:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = true;
                    cmdCancelar.Enabled = true;
                    //Controles                    
                    txtLogin.Enabled = true;
                    txtNombre.Enabled = true;
                    txtApellido.Enabled = true;
                    txtContrasena.Enabled = true;
                    dtpFechaVencimiento.Enabled = true;
                    cboRoles.Enabled = true;
                    chkActivo.Enabled = true;
                    break;
            }

        }

        private void CargarDatos(Int16 IDPk)
        {
            DataTable dtRoles = new DataTable();
            Parametros.Add("@idusuario", MySqlDbType.Int16).Value = IDPk;


            dtRoles = cls.RetornarDatos("usuarios", "*", Parametros, "idusuario=@idusuario");
            try
            {
                if (dtRoles.Rows.Count == 1)
                {
                    txtIDUsuario.Text = dtRoles.Rows[0]["idusuario"].ToString();
                    txtLogin.Text = dtRoles.Rows[0]["login"].ToString();
                    txtNombre.Text = dtRoles.Rows[0]["nombre"].ToString();
                    txtApellido.Text = dtRoles.Rows[0]["apellido"].ToString();
                    dtpFechaVencimiento.SelectedDate = Convert.ToDateTime(dtRoles.Rows[0]["vencimientocontrasena"].ToString());
                    cboRoles.SelectedValue = dtRoles.Rows[0]["idrol"].ToString();

                    if (dtRoles.Rows[0]["activousuario"].ToString() == "Activo")
                    {
                        chkActivo.Checked = true;
                    }
                    else if (dtRoles.Rows[0]["activousuario"].ToString() == "Inactivo")
                    {
                        chkActivo.Checked = false;
                    }
                    hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                    Estados();
                }

                dtRoles.Dispose();
                Parametros.Clear();
            }
            catch (Exception ex)
            {
                cls.LevantarError(ex);
            }

        }

        private void LimpiarControles()
        {
            txtIDUsuario.Text = "";
            txtLogin.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtContrasena.Text = "";
            chkActivo.Checked = false;
            dtpFechaVencimiento.SelectedDate = null;
        }
        #endregion

        protected void grdListadoUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            string IDPk = Convert.ToString(grdListadoUsuarios.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoUsuarios.SelectedIndexes[0])]["idusuario"]);
            CargarDatos(Convert.ToInt16(IDPk));     
        }

    }
}