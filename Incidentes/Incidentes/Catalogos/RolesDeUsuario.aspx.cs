﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;

namespace Incidentes.Catalogos
{
    public partial class RolesDeUsuario : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };

        const int IDOpcion = 12;
        enumEstados Estado;
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            cls.onError += EventoError;
            lblMensajes.Text = "";
            if (!IsPostBack)
            {
                Estados();
                Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
            }
        }

        
        protected void cmdNuevo_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Nuevo);
            Estados();
        }


        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    byte Activo;
                    Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
                    Activo = (chkActivo.Checked ? (byte)1 : (byte)2);
                                        
                    Int16 IDRol = (Estado == enumEstados.Nuevo ? Convert.ToInt16(cls.Max("roles", "idrol")) : Convert.ToInt16(txtIDRol.Text));
                    
                    Parametros.Add("@idrol", MySqlDbType.Int16).Value = IDRol;
                    Parametros.Add("@rol", MySqlDbType.VarChar, 20).Value = txtRol.Text;
                    Parametros.Add("@activorol", MySqlDbType.Enum).Value = (Estado == enumEstados.Nuevo ? 1 : Activo);

                    if (Estado == enumEstados.Nuevo)
                    {
                        if (cls.IngresarDatosNuevos("roles", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            txtIDRol.Text = Convert.ToString(IDRol);
                            chkActivo.Checked = true;
                            grdListadoRoles.DataBind();
                        }
                    }
                    else if (Estado == enumEstados.Editando)
                    {
                        if (cls.ActualizarDatos("roles", "idrol", Convert.ToInt16(txtIDRol.Text), Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            grdListadoRoles.DataBind();
                        }
                    }
                    Parametros.Clear();

                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }                
            }
        }

        protected void grdListadoRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            string IDRolSeleccionado = Convert.ToString(grdListadoRoles.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoRoles.SelectedIndexes[0])]["idrol"]);
            CargarDatos(Convert.ToInt16(IDRolSeleccionado));        
        }


        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
            Estados();
            txtIDRol.Text = "";
            txtRol.Text = "";
            chkActivo.Checked = false;

        }
        
        protected void cmdAgregarAcceso_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Parametros.Add("@idrol", MySqlDbType.Int16).Value = Convert.ToInt16( txtIDRol.Text);
                Parametros.Add("@idopcion", MySqlDbType.Int16).Value = Convert.ToInt16(cboOpciones.SelectedValue);

                if (cls.IngresarDatosNuevos("opcionesporrol", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                {
                    grdOpciones.Rebind();
                }
            }
        }


        #region Funciones
        private void Estados()
        {
            Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);

            switch (Estado)
            {
                case enumEstados.SinEstado:
                    //Botones
                    cmdNuevo.Enabled = true;
                    cmdGuardar.Enabled = false;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = false;
                    //Controles
                    txtRol.Enabled = false;
                    cboOpciones.Enabled = false;
                    cmdAgregarAcceso.Enabled = false;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Nuevo:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtRol.Enabled = true;
                    cboOpciones.Enabled = false;
                    cmdAgregarAcceso.Enabled = false;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Editando:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = true;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtRol.Enabled = true;
                    cboOpciones.Enabled = true;
                    cmdAgregarAcceso.Enabled = true;
                    chkActivo.Enabled = true;
                    break;
            }

        }

        private void CargarDatos(Int16 IDRol)
        {
            DataTable dtRoles = new DataTable();
            Parametros.Add("@idrol", MySqlDbType.Int16).Value = IDRol;

            dtRoles = cls.RetornarDatos("roles", "*", Parametros, "idrol=@idrol");

            if (dtRoles.Rows.Count == 1)
            {
                txtIDRol.Text = dtRoles.Rows[0]["idrol"].ToString();
                txtRol.Text = dtRoles.Rows[0]["rol"].ToString();
                if(dtRoles.Rows[0]["activorol"].ToString()== "Activo")
                {
                    chkActivo.Checked = true;                        
                }
                else if (dtRoles.Rows[0]["activorol"].ToString() == "Inactivo")
                {
                    chkActivo.Checked =false;  
                }
                hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                Estados();
            }

            dtRoles.Dispose();
            Parametros.Clear();


        }
        #endregion
        
        private void EventoError(string DescripcionError)
        {
            lblMensajes.Text = DescripcionError;
        }

        protected void grdOpciones_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                GridDataItem item = (GridDataItem)e.Item;
                Int16 IDOpcionPorRol = Convert.ToInt16(item.GetDataKeyValue("idopcionporrol").ToString());

                if (cls.EliminarDatos("opcionesporrol", "idopcionporrol", IDOpcionPorRol, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                {
                    grdOpciones.Rebind();
                }
            }
            catch (Exception ex)
            {
                cls.LevantarError(ex);
            }
        }

    }
}