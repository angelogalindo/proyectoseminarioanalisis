﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Municipios.aspx.cs" Inherits="Incidentes.Catalogos.Municipios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Municipios</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    <asp:HiddenField ID="hdnEstado" Value="0" runat="server" />

    <div class="row">
        <div class="col-lg-6">
            <fieldset class="form-group">     
                <legend>Ingreso y Edición</legend>
                <div class="row">
                    <div class="col-lg-4">
                        <label>IdMunicipio</label>
                        <asp:TextBox ID="txtIDMunicipio" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Municipio</label>
                        <asp:TextBox ID="txtMunicipio" CssClass="form-control" MaxLength="30"  runat="server"></asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        <label>Departamento</label><br />
                        <telerik:RadComboBox ID="cboDepartamentos" Filter="Contains" Style="width: 60%; min-width: 150px" DataTextField="departamento" DataValueField="iddepartamento" runat="server" DataSourceID="sqlDepartamentos"></telerik:RadComboBox>
                        <asp:SqlDataSource runat="server" ID="sqlDepartamentos" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 AS iddepartamento, 'Seleccione un departamento' AS departamento UNION SELECT iddepartamento, departamento FROM departamentos"></asp:SqlDataSource>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                            <Icon PrimaryIconCssClass="rbAdd"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                            <Icon PrimaryIconCssClass="rbSave"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                                <Icon PrimaryIconCssClass="rbRemove"></Icon>
                        </telerik:RadButton>
                        <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                            <Icon PrimaryIconCssClass="rbCancel"></Icon>
                        </telerik:RadButton>
                    </div> 
                </div>
                        
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <fieldset>
                <legend>Listado de departamentos</legend>
                <telerik:RadGrid ID="grdListadoMunicipios" runat="server" DataSourceID="sqlListadoMunicipios"
                    OnSelectedIndexChanged="grdListadoMunicipios_SelectedIndexChanged">
                    <MasterTableView DataKeyNames="idmunicipio" DataSourceID="sqlListadoMunicipios" AutoGenerateColumns="False">
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="idmunicipio" Display="false" ReadOnly="True" HeaderText="idmunicipio" SortExpression="idmunicipio" UniqueName="idmunicipio" DataType="System.Int32" FilterControlAltText="Filter idmunicipio column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="municipio" HeaderText="Municipio" SortExpression="municipio" UniqueName="municipio" FilterControlAltText="Filter municipio column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="departamento" HeaderText="Departamento" SortExpression="departamento" UniqueName="departamento" FilterControlAltText="Filter departamento column"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>

                <asp:SqlDataSource runat="server" ID="sqlListadoMunicipios" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idmunicipio, municipio, departamento FROM view_municipios"></asp:SqlDataSource>
            </fieldset>
        </div>
    </div>

</asp:Content>
