﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;


namespace Incidentes.Catalogos
{
    public partial class Municipios : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };

        const int IDOpcion = 10;
        enumEstados Estado;
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            cls.onError += EventoError;
            lblMensajes.Text = "";
            if (!IsPostBack)
            {
                Estados();
                Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
            }
        }

        protected void cmdNuevo_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Nuevo);
            Estados();
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {                   
                    Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
                 
                    Int16 IDPk = (Estado == enumEstados.Nuevo ? Convert.ToInt16(cls.Max("municipios", "idmunicipio")) : Convert.ToInt16(txtIDMunicipio.Text));

                    Parametros.Add("@idmunicipio", MySqlDbType.Int16).Value = IDPk;
                    Parametros.Add("@municipio", MySqlDbType.VarChar, 30).Value = txtMunicipio.Text;
                    Parametros.Add("@iddepartamento", MySqlDbType.Int16).Value = cboDepartamentos.SelectedValue;

                    if (Estado == enumEstados.Nuevo)
                    {
                        if (cls.IngresarDatosNuevos("municipios", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {

                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            txtIDMunicipio.Text = Convert.ToString(IDPk);
                            grdListadoMunicipios.DataBind();
                        }
                    }
                    else if (Estado == enumEstados.Editando)
                    {
                        if (cls.ActualizarDatos("municipios", "idmunicipio", Convert.ToInt16(txtIDMunicipio.Text), Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            grdListadoMunicipios.DataBind();
                        }
                    }
                    Parametros.Clear();

                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    Int16 IDPk = Convert.ToInt16(txtIDMunicipio.Text);
                    if (cls.EliminarDatos("municipios", "idmunicipio", IDPk, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                    {
                        hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
                        Estados();
                        LimpiarControles();
                        grdListadoMunicipios.Rebind();
                    }
                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
            Estados();
            LimpiarControles();
        }

        protected void grdListadoMunicipios_SelectedIndexChanged(object sender, EventArgs e)
        {
            string IDPk = Convert.ToString(grdListadoMunicipios.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoMunicipios.SelectedIndexes[0])]["idmunicipio"]);
            CargarDatos(Convert.ToInt16(IDPk));  
        }

        private void EventoError(string DescripcionError)
        {
            lblMensajes.Text = DescripcionError;
        }

        #region Funciones
        private void Estados()
        {
            Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);

            switch (Estado)
            {
                case enumEstados.SinEstado:
                    //Botones
                    cmdNuevo.Enabled = true;
                    cmdGuardar.Enabled = false;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = false;
                    //Controles                    
                    txtMunicipio.Enabled = false;
                    cboDepartamentos.Enabled = false;
                    break;
                case enumEstados.Nuevo:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtMunicipio.Enabled = true;
                    cboDepartamentos.Enabled = true;
                    break;
                case enumEstados.Editando:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = true;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtMunicipio.Enabled = true;
                    cboDepartamentos.Enabled = true;
                    break;
            }

        }

        private void CargarDatos(Int16 IDPk)
        {
            DataTable dtRoles = new DataTable();
            Parametros.Add("@idmunicipio", MySqlDbType.Int16).Value = IDPk;


            dtRoles = cls.RetornarDatos("municipios", "*", Parametros, "idmunicipio=@idmunicipio");
            try
            {
                if (dtRoles.Rows.Count == 1)
                {
                    txtIDMunicipio.Text = dtRoles.Rows[0]["idmunicipio"].ToString();
                    txtMunicipio.Text = dtRoles.Rows[0]["municipio"].ToString();
                    cboDepartamentos.SelectedValue = dtRoles.Rows[0]["iddepartamento"].ToString();
                   
                    hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                    Estados();
                }

                dtRoles.Dispose();
                Parametros.Clear();
            }
            catch (Exception ex)
            {
                cls.LevantarError(ex);
            }

        }

        private void LimpiarControles()
        {
            txtIDMunicipio.Text = "";
            txtMunicipio.Text = "";
            cboDepartamentos.SelectedValue = "0";

        }
        #endregion
    }
}