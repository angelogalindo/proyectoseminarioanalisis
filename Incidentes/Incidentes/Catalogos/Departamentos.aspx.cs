﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;

namespace Incidentes.Catalogos
{
    public partial class Departamentos : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };

        const int IDOpcion = 9;
        enumEstados Estado;
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            cls.onError += EventoError;
            lblMensajes.Text = "";
            if (!IsPostBack)
            {
                Estados();
                Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
            }
        }

        protected void cmdNuevo_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Nuevo);
            Estados();
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                 
                    Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);

                    Int16 IDPk = (Estado == enumEstados.Nuevo ? Convert.ToInt16(cls.Max("departamentos", "iddepartamento")) : Convert.ToInt16(txtIDDepartamento.Text));

                    Parametros.Add("@iddepartamento", MySqlDbType.Int16).Value = IDPk;
                    Parametros.Add("@departamento", MySqlDbType.VarChar, 20).Value = txtDepartamento.Text;
                    

                    if (Estado == enumEstados.Nuevo)
                    {
                        if (cls.IngresarDatosNuevos("departamentos", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {

                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            txtIDDepartamento.Text = Convert.ToString(IDPk);
                            
                            grdListadoDepartamentos.DataBind();
                        }
                    }
                    else if (Estado == enumEstados.Editando)
                    {
                        if (cls.ActualizarDatos("departamentos", "iddepartamento", Convert.ToInt16(txtIDDepartamento.Text), Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            grdListadoDepartamentos.DataBind();
                        }
                    }
                    Parametros.Clear();

                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    Int16 IDPk = Convert.ToInt16(txtIDDepartamento.Text);
                    if (cls.EliminarDatos("departamentos", "iddepartamento", IDPk, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                    {
                        hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
                        Estados();
                        LimpiarControles();
                        grdListadoDepartamentos.Rebind();
                    }
                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
            Estados();
            LimpiarControles();
        }

        protected void grdListadoDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            string IDPk = Convert.ToString(grdListadoDepartamentos.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoDepartamentos.SelectedIndexes[0])]["iddepartamento"]);
            CargarDatos(Convert.ToInt16(IDPk)); 
        }

        private void EventoError(string DescripcionError)
        {
            lblMensajes.Text = DescripcionError;
        }

        #region Funciones
        private void Estados()
        {
            Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);

            switch (Estado)
            {
                case enumEstados.SinEstado:
                    //Botones
                    cmdNuevo.Enabled = true;
                    cmdGuardar.Enabled = false;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = false;
                    //Controles                    
                    txtDepartamento.Enabled = false;
                    break;
                case enumEstados.Nuevo:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtDepartamento.Enabled = true;
                    break;
                case enumEstados.Editando:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = true;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtDepartamento.Enabled = true;
                    break;
            }

        }

        private void CargarDatos(Int16 IDPk)
        {
            DataTable dtRoles = new DataTable();
            Parametros.Add("@iddepartamento", MySqlDbType.Int16).Value = IDPk;


            dtRoles = cls.RetornarDatos("departamentos", "*", Parametros, "iddepartamento=@iddepartamento");
            try
            {
                if (dtRoles.Rows.Count == 1)
                {
                    txtIDDepartamento.Text = dtRoles.Rows[0]["iddepartamento"].ToString();
                    txtDepartamento.Text = dtRoles.Rows[0]["departamento"].ToString();
                                      
                    hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                    Estados();
                }

                dtRoles.Dispose();
                Parametros.Clear();
            }
            catch (Exception ex)
            {
                cls.LevantarError(ex);
            }

        }

        private void LimpiarControles()
        {
            txtIDDepartamento.Text = "";
            txtDepartamento.Text = "";            

        }
        #endregion
    }
}