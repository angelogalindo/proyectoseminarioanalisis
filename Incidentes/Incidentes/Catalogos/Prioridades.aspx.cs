﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Agregar a todo
using CAD;
using MySql.Data.MySqlClient;
using System.Data;
using Telerik.Web.UI;

namespace Incidentes.Catalogos
{
    public partial class Prioridades : System.Web.UI.Page
    {
        public enum enumEstados
        {
            SinEstado,
            Nuevo,
            Editando
        };

        const int IDOpcion = 8;
        enumEstados Estado;
        CAD_Mysql cls = new CAD_Mysql();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        protected void Page_Load(object sender, EventArgs e)
        {
            cls.onError += EventoError;
            lblMensajes.Text = "";
            if (!IsPostBack)
            {
                Estados();
                Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
            }
        }

        protected void cmdNuevo_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Nuevo);
            Estados();
        }

        protected void cmdGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    byte Activo;
                    Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);
                    Activo = (chkActivo.Checked ? (byte)1 : (byte)2);

                    Int16 IDPk = (Estado == enumEstados.Nuevo ? Convert.ToInt16(cls.Max("prioridades", "idprioridad")) : Convert.ToInt16(txtIDPrioridad.Text));

                    Parametros.Add("@idprioridad", MySqlDbType.Int16).Value = IDPk;
                    Parametros.Add("@prioridad", MySqlDbType.VarChar, 20).Value = txtPrioridad.Text;
                    Parametros.Add("@activoprioridad", MySqlDbType.Enum).Value = (Estado == enumEstados.Nuevo ? 1 : Activo);

                    if (Estado == enumEstados.Nuevo)
                    {
                        if (cls.IngresarDatosNuevos("prioridades", Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {

                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            txtIDPrioridad.Text = Convert.ToString(IDPk);
                            chkActivo.Checked = true;
                            grdListadoPrioridades.DataBind();
                        }
                    }
                    else if (Estado == enumEstados.Editando)
                    {
                        if (cls.ActualizarDatos("prioridades", "idprioridad", Convert.ToInt16(txtIDPrioridad.Text), Parametros, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                        {
                            hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                            Estados();
                            grdListadoPrioridades.DataBind();
                        }
                    }
                    Parametros.Clear();

                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdEliminar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    Int16 IDPk = Convert.ToInt16(txtIDPrioridad.Text);
                    if (cls.EliminarDatos("prioridades", "idprioridad", IDPk, Convert.ToInt16(Session["IDUsuario"]), IDOpcion))
                    {
                        hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
                        Estados();
                        LimpiarControles();
                        grdListadoPrioridades.Rebind();
                    }
                }
                catch (Exception ex)
                {
                    cls.LevantarError(ex);
                }
            }
        }

        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            hdnEstado.Value = Convert.ToString((Int16)enumEstados.SinEstado);
            Estados();
            LimpiarControles();
        }

        protected void grdListadoPrioridades_SelectedIndexChanged(object sender, EventArgs e)
        {
            string IDPk = Convert.ToString(grdListadoPrioridades.MasterTableView.DataKeyValues[Convert.ToInt16(grdListadoPrioridades.SelectedIndexes[0])]["idprioridad"]);
            CargarDatos(Convert.ToInt16(IDPk));   
        }

        private void EventoError(string DescripcionError)
        {
            lblMensajes.Text = DescripcionError;
        }

        #region Funciones
        private void Estados()
        {
            Estado = (enumEstados)Convert.ToInt16(hdnEstado.Value);

            switch (Estado)
            {
                case enumEstados.SinEstado:
                    //Botones
                    cmdNuevo.Enabled = true;
                    cmdGuardar.Enabled = false;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = false;
                    //Controles                    
                    txtPrioridad.Enabled = false;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Nuevo:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = false;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtPrioridad.Enabled = true;
                    chkActivo.Enabled = false;
                    break;
                case enumEstados.Editando:
                    //Botones
                    cmdNuevo.Enabled = false;
                    cmdGuardar.Enabled = true;
                    cmdEliminar.Enabled = true;
                    cmdCancelar.Enabled = true;
                    //Controles
                    txtPrioridad.Enabled = true;
                    chkActivo.Enabled = true;
                    break;
            }

        }

        private void CargarDatos(Int16 IDPk)
        {
            DataTable dtRoles = new DataTable();
            Parametros.Add("@idprioridad", MySqlDbType.Int16).Value = IDPk;


            dtRoles = cls.RetornarDatos("prioridades", "*", Parametros, "idprioridad=@idprioridad");
            try
            {
                if (dtRoles.Rows.Count == 1)
                {
                    txtIDPrioridad.Text = dtRoles.Rows[0]["idprioridad"].ToString();
                    txtPrioridad.Text = dtRoles.Rows[0]["prioridad"].ToString();

                    if (dtRoles.Rows[0]["activoprioridad"].ToString() == "Activo")
                    {
                        chkActivo.Checked = true;
                    }
                    else if (dtRoles.Rows[0]["activoprioridad"].ToString() == "Inactivo")
                    {
                        chkActivo.Checked = false;
                    }
                    hdnEstado.Value = Convert.ToString((Int16)enumEstados.Editando);
                    Estados();
                }

                dtRoles.Dispose();
                Parametros.Clear();
            }
            catch (Exception ex)
            {
                cls.LevantarError(ex);
            }

        }

        private void LimpiarControles()
        {
            txtIDPrioridad.Text = "";
            txtPrioridad.Text = "";
            chkActivo.Checked = false;

        }
        #endregion
    }
}