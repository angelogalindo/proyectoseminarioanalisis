﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RolesDeUsuario.aspx.cs" Inherits="Incidentes.Catalogos.RolesDeUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="font-weight:bold">Roles</h2>
    <br />
    <asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
    
        
    <div class="row">
        <div class="col-lg-5">    
            <fieldset class="form-group"> 
                <legend>Ingreso y Edicion</legend>

                <asp:HiddenField ID="hdnEstado" Value="0" runat="server" />   
                <label>IdRol</label>
                <asp:TextBox ID="txtIDRol" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                <label>Descripción Rol</label>
                <asp:TextBox ID="txtRol" CssClass="form-control" MaxLength="20"  runat="server"></asp:TextBox>
                <asp:CheckBox ID="chkActivo" Text="Rol Activo" Checked="false" runat="server" />
                <br /><br />
                <label>Accesos</label>
                <br />
                <telerik:RadComboBox ID="cboOpciones" Filter="Contains" style="width:60%; min-width:150px" DataTextField="opcion" DataValueField="idopcion" runat="server" DataSourceID="sqlOpciones"></telerik:RadComboBox>
                <telerik:RadButton ID="cmdAgregarAcceso" runat="server" Text="Agregar Acceso" OnClick="cmdAgregarAcceso_Click"></telerik:RadButton>
                
                <asp:SqlDataSource runat="server" ID="sqlOpciones" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idopcion, concat(opcion,' [', grupo,']') as opcion FROM opciones"></asp:SqlDataSource>
                <br /><br />
                <telerik:RadGrid ID="grdOpciones" runat="server" DataSourceID="sqlOpcionesRol" OnDeleteCommand="grdOpciones_DeleteCommand" GroupPanelPosition="Top">
                    <MasterTableView DataSourceID="sqlOpcionesRol" AutoGenerateColumns="False" DataKeyNames="idopcionporrol">
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/delete.png"
                            CommandName="Delete" UniqueName="DeleteColumn" ConfirmDialogType="Classic" ConfirmText="¿Desea eliminar el registro?">
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="idopcionporrol" ReadOnly="true" Display="false" HeaderText="idopcionporrol" SortExpression="idopcionporrol" UniqueName="idopcionporrol" DataType="System.Int32" FilterControlAltText="Filter idopcionporrol column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="opcion" HeaderText="Opcion" SortExpression="opcion" UniqueName="opcion" FilterControlAltText="Filter opcion column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="grupo" HeaderText="Grupo" SortExpression="grupo" UniqueName="grupo" FilterControlAltText="Filter grupo column"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:SqlDataSource runat="server" ID="sqlOpcionesRol" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idopcionporrol,opcion,grupo FROM view_opcionesporrol WHERE (idrol = @idrol)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtIDRol" Name="idrol" PropertyName="text" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />     
                <div>
                    <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                        <Icon PrimaryIconCssClass="rbAdd"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                        <Icon PrimaryIconCssClass="rbSave"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar">
                            <Icon PrimaryIconCssClass="rbRemove"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                        <Icon PrimaryIconCssClass="rbCancel"></Icon>
                    </telerik:RadButton>
                </div> 
            </fieldset>    
        </div>            
        <div class="col-lg-7">
            <fieldset>
                <legend>Listado de Roles</legend>
                <telerik:RadGrid ID="grdListadoRoles" runat="server"  DataSourceID="sqlListadoRoles" OnSelectedIndexChanged="grdListadoRoles_SelectedIndexChanged"  >
                    <MasterTableView DataKeyNames="idrol" DataSourceID="sqlListadoRoles" AutoGenerateColumns="False">
                        <Columns>
                            <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="15px" />
                            </telerik:GridButtonColumn>
                            <telerik:GridBoundColumn DataField="idrol" Display="false" ReadOnly="True" HeaderText="idrol" SortExpression="idrol" UniqueName="idrol" DataType="System.Int32" FilterControlAltText="Filter idrol column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="rol" HeaderText="Rol" SortExpression="rol" UniqueName="rol" FilterControlAltText="Filter rol column"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="activorol" HeaderText="Estado" SortExpression="activorol" UniqueName="activorol" FilterControlAltText="Filter activorol column"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>

                <asp:SqlDataSource runat="server" ID="sqlListadoRoles" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT idrol, rol, activorol FROM roles where idrol>0"></asp:SqlDataSource>
            </fieldset>
        </div>
    </div>   
    
    
    
</asp:Content>
