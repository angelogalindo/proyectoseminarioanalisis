﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Departamentos.aspx.cs" Inherits="Incidentes.Catalogos.Departamentos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2 style="font-weight:bold">Departamentos</h2>
<br />
<asp:Label ID="lblMensajes" Font-Bold="true" ForeColor="Red" runat="server" Text=""></asp:Label>
<asp:HiddenField ID="hdnEstado" Value="0" runat="server" />

<div class="row">
    <div class="col-lg-6">
        <fieldset class="form-group">     
            <legend>Ingreso y Edición</legend>
            <div class="row">
                <div class="col-lg-4">
                    <label>IdDepartamento</label>
                    <asp:TextBox ID="txtIDDepartamento" ReadOnly="true" Style="width:20%;min-width:100px;" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-lg-8">
                    <label>Departamento</label>
                    <asp:TextBox ID="txtDepartamento" CssClass="form-control" MaxLength="30"  runat="server"></asp:TextBox>
                </div>               
            </div>
            <br />
            <div class="row">
                <div class="col-lg-12">
                    <telerik:RadButton ID="cmdNuevo" runat="server"  Text="Nuevo" OnClick="cmdNuevo_Click">
                        <Icon PrimaryIconCssClass="rbAdd"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="cmdGuardar" runat="server" Text="Guardar" OnClick="cmdGuardar_Click">
                        <Icon PrimaryIconCssClass="rbSave"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="cmdEliminar" runat="server" Text="Eliminar" OnClick="cmdEliminar_Click">
                            <Icon PrimaryIconCssClass="rbRemove"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="cmdCancelar" runat="server" Text="Cancelar" OnClick="cmdCancelar_Click">
                        <Icon PrimaryIconCssClass="rbCancel"></Icon>
                    </telerik:RadButton>
                </div> 
            </div>
                        
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <fieldset>
            <legend>Listado de Categorías</legend>
            <telerik:RadGrid ID="grdListadoDepartamentos" runat="server" DataSourceID="sqlListadoDepartamentos" 
                OnSelectedIndexChanged="grdListadoDepartamentos_SelectedIndexChanged">
                <MasterTableView DataKeyNames="iddepartamento" DataSourceID="sqlListadoDepartamentos" AutoGenerateColumns="False">
                    <Columns>
                        <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../images/ok.png" CommandName="Select" UniqueName="SelectColumn" >
                                    <HeaderStyle Width="40px" />
                            </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="iddepartamento" Display="false" ReadOnly="True" HeaderText="iddepartamento" SortExpression="iddepartamento" UniqueName="iddepartamento" DataType="System.Int32" FilterControlAltText="Filter iddepartamento column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="departamento" HeaderText="Departamento" SortExpression="departamento" UniqueName="departamento" FilterControlAltText="Filter departamento column"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>

            <asp:SqlDataSource runat="server" ID="sqlListadoDepartamentos" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT iddepartamento, departamento FROM departamentos"></asp:SqlDataSource>
        </fieldset>
    </div>
</div>
</asp:Content>
