﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CAD;
using MySql.Data.MySqlClient;
using System.Data;

namespace Incidentes
{
    public partial class Site : System.Web.UI.MasterPage
    {
        CAD_Mysql cls = new CAD_Mysql();
        DataTable dtOpciones = new DataTable();
        DataTable dtCatalogos = new DataTable();
        MySqlParameterCollection Parametros = new MySqlCommand().Parameters;

        public string             
            formatoMenu = "<li ><a href='{0}'><span class='fa fa-caret-right'></span> {1}</a></li>",
            mnuOpciones= "",
            mnuCatalogos = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IDUsuario"] == null)
            {
                Response.Redirect(ResolveClientUrl("Login.aspx"));
            }
            else 
            {
                ConstruirMenu();                   
            }
        }

        protected void cmdCerrarSesion_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect(ResolveClientUrl("Login.aspx"));
        }

        private void ConstruirMenu()
        {
            int i = 0;
            if (Convert.ToInt16(Session["IDUsuario"]) == 0) {
                //Obtiene las opciones de operaciones
                Parametros.Add("@grupo", MySqlDbType.Enum).Value = 1;
                dtOpciones = cls.RetornarDatos("opciones", "opcion,concat('~/',grupo , '/',formularioopcion) as formularioopcion", Parametros, "grupo=@grupo and enmenu=1");
                for(i=0;i < dtOpciones.Rows.Count; i++){
                    mnuOpciones += String.Format(formatoMenu, ResolveClientUrl(Convert.ToString(dtOpciones.Rows[i]["formularioopcion"])), dtOpciones.Rows[i]["opcion"]);
                }
                //Obtiene las opciones de catalogos
                Parametros["@grupo"].Value = 2;
                dtCatalogos = cls.RetornarDatos("opciones", "opcion,concat('~/',grupo , '/',formularioopcion) as formularioopcion", Parametros, "grupo=@grupo");
                for (i = 0; i < dtCatalogos.Rows.Count; i++)
                {
                    mnuCatalogos += String.Format(formatoMenu,ResolveClientUrl(Convert.ToString(dtCatalogos.Rows[i]["formularioopcion"])), dtCatalogos.Rows[i]["opcion"]);
                }
                dtOpciones.Dispose();
                dtCatalogos.Dispose();
                Parametros.Clear();
            }
            else
            {
                //Obtiene las opciones de operaciones
                Parametros.Add("@idusuario", MySqlDbType.Int16).Value = Session["IDUsuario"];
                Parametros.Add("@grupo", MySqlDbType.Enum).Value = 1;

                dtOpciones = cls.RetornarDatos("view_verificarpermisos", "opcion,ruta", Parametros, "idusuario=@idusuario and grupo=@grupo and enmenu=1");
                for (i = 0; i < dtOpciones.Rows.Count; i++)
                {
                    mnuOpciones += String.Format(formatoMenu, ResolveClientUrl(Convert.ToString(dtOpciones.Rows[i]["ruta"])), dtOpciones.Rows[i]["opcion"]);
                }
                //Obtiene las opciones de catalogos
                Parametros["@grupo"].Value = 2;
                dtCatalogos = cls.RetornarDatos("view_verificarpermisos", "opcion,ruta", Parametros, "idusuario=@idusuario and grupo=@grupo");
                for (i = 0; i < dtCatalogos.Rows.Count; i++)
                {
                    mnuCatalogos += String.Format(formatoMenu, ResolveClientUrl(Convert.ToString(dtCatalogos.Rows[i]["ruta"])), dtCatalogos.Rows[i]["opcion"]);
                }
                dtOpciones.Dispose();
                dtCatalogos.Dispose();
                Parametros.Clear();
            }
        }

    }
}