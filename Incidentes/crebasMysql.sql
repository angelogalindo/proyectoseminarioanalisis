/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     09/11/2015 11:19:47 p. m.                    */
/*==============================================================*/


drop table if exists bitacora;

drop table if exists categorias;

drop table if exists comentariosincidente;

drop table if exists departamentos;

drop table if exists estados;

drop table if exists incidentes;

drop table if exists municipios;

drop table if exists opciones;

drop table if exists opcionesporrol;

drop table if exists prioridades;

drop table if exists roles;

drop table if exists solicitantes;

drop table if exists subcategorias;

drop table if exists usuarios;

/*==============================================================*/
/* Table: bitacora                                              */
/*==============================================================*/
create table bitacora
(
   idbitacora           int not null,
   idopcion             int not null,
   idusuario            int not null,
   idregistro           varchar(5),
   fechayhora           datetime,
   primary key (idbitacora)
);

/*==============================================================*/
/* Table: categorias                                            */
/*==============================================================*/
create table categorias
(
   idcategoria          int not null,
   categoria            varchar(20),
   activocategoria      bool,
   primary key (idcategoria)
);

/*==============================================================*/
/* Table: comentariosincidente                                  */
/*==============================================================*/
create table comentariosincidente
(
   idcomentario         int not null,
   idincidente          int not null,
   idusuario            int not null,
   comentario           varchar(300),
   fechahoracomentario  datetime,
   primary key (idcomentario)
);

/*==============================================================*/
/* Table: departamentos                                         */
/*==============================================================*/
create table departamentos
(
   iddepartamento       int not null,
   departamento         varchar(20),
   primary key (iddepartamento)
);

/*==============================================================*/
/* Table: estados                                               */
/*==============================================================*/
create table estados
(
   idestado             int not null,
   estado               varchar(15),
   activoestado         bool,
   primary key (idestado)
);

/*==============================================================*/
/* Table: incidentes                                            */
/*==============================================================*/
create table incidentes
(
   idincidente          int not null,
   idsubcategoria       int not null,
   idestado             int not null,
   idusuario            int not null,
   idsolicitante        int not null,
   idprioridad          int not null,
   asunto               varchar(30),
   descripcion          varchar(300),
   fechahoraincidente   datetime,
   fechahoracierre      datetime,
   primary key (idincidente)
);

/*==============================================================*/
/* Table: municipios                                            */
/*==============================================================*/
create table municipios
(
   idmunicipio          int not null,
   iddepartamento       int not null,
   attributde_15        varchar(20),
   primary key (idmunicipio)
);

/*==============================================================*/
/* Table: opciones                                              */
/*==============================================================*/
create table opciones
(
   idopcion             int not null,
   opcion               varchar(15),
   formularioopcion     varchar(15),
   activoopcion         bool,
   primary key (idopcion)
);

/*==============================================================*/
/* Table: opcionesporrol                                        */
/*==============================================================*/
create table opcionesporrol
(
   idopcionporrol       int not null,
   idrol                int not null,
   idopcion             int not null,
   primary key (idopcionporrol)
);

/*==============================================================*/
/* Table: prioridades                                           */
/*==============================================================*/
create table prioridades
(
   idprioridad          int not null,
   prioridad            varchar(15),
   activoprioridad      bool,
   primary key (idprioridad)
);

/*==============================================================*/
/* Table: roles                                                 */
/*==============================================================*/
create table roles
(
   idrol                int not null,
   rol                  varchar(20),
   activorol            bool,
   primary key (idrol)
);

/*==============================================================*/
/* Table: solicitantes                                          */
/*==============================================================*/
create table solicitantes
(
   idsolicitante        int not null,
   idmunicipio          int not null,
   solicitante          varchar(20),
   telefonosolicitante  varchar(8),
   direccionsolicitante varchar(20),
   activosolicitante    bool,
   primary key (idsolicitante)
);

/*==============================================================*/
/* Table: subcategorias                                         */
/*==============================================================*/
create table subcategorias
(
   idsubcategoria       int not null,
   idcategoria          int not null,
   subcategoria         varchar(20),
   activosubcategoria   bool,
   primary key (idsubcategoria)
);

/*==============================================================*/
/* Table: usuarios                                              */
/*==============================================================*/
create table usuarios
(
   idusuario            int not null,
   idrol                int not null,
   login                varchar(10),
   contrasena           varchar(40),
   vencimientocontrasena date,
   nombre               varchar(20),
   apellido             varchar(20),
   activousuario        bool,
   primary key (idusuario)
);

alter table bitacora add constraint fk_fk_opciones_bitacora foreign key (idopcion)
      references opciones (idopcion) on delete restrict on update restrict;

alter table bitacora add constraint fk_fk_usuarios_bitacora foreign key (idusuario)
      references usuarios (idusuario) on delete restrict on update restrict;

alter table comentariosincidente add constraint fk_fk_incidentes_comentariosincidentes foreign key (idincidente)
      references incidentes (idincidente) on delete restrict on update restrict;

alter table comentariosincidente add constraint fk_fk_usuarios_comentariosincidente foreign key (idusuario)
      references usuarios (idusuario) on delete restrict on update restrict;

alter table incidentes add constraint fk_fk_estados_incidentes foreign key (idestado)
      references estados (idestado) on delete restrict on update restrict;

alter table incidentes add constraint fk_fk_prioridad_incidentes foreign key (idprioridad)
      references prioridades (idprioridad) on delete restrict on update restrict;

alter table incidentes add constraint fk_fk_solicitante_incidentes foreign key (idsolicitante)
      references solicitantes (idsolicitante) on delete restrict on update restrict;

alter table incidentes add constraint fk_fk_subcategorias_incidentes foreign key (idsubcategoria)
      references subcategorias (idsubcategoria) on delete restrict on update restrict;

alter table incidentes add constraint fk_fk_usuarios_incidentes foreign key (idusuario)
      references usuarios (idusuario) on delete restrict on update restrict;

alter table municipios add constraint fk_fk_departamento_municipios foreign key (iddepartamento)
      references departamentos (iddepartamento) on delete restrict on update restrict;

alter table opcionesporrol add constraint fk_fk_opciones_opcionporrol foreign key (idopcion)
      references opciones (idopcion) on delete restrict on update restrict;

alter table opcionesporrol add constraint fk_fk_roles_opcionesporrol foreign key (idrol)
      references roles (idrol) on delete restrict on update restrict;

alter table solicitantes add constraint fk_fk_municipios_incidentes foreign key (idmunicipio)
      references municipios (idmunicipio) on delete restrict on update restrict;

alter table subcategorias add constraint fk_fk_categorias_subcategorias foreign key (idcategoria)
      references categorias (idcategoria) on delete restrict on update restrict;

alter table usuarios add constraint fk_fk_roles_usuario foreign key (idrol)
      references roles (idrol) on delete restrict on update restrict;

