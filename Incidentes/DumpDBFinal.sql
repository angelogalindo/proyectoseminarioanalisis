-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.17 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para incidentes
CREATE DATABASE IF NOT EXISTS `incidentes` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `incidentes`;


-- Volcando estructura para tabla incidentes.bitacora
CREATE TABLE IF NOT EXISTS `bitacora` (
  `idbitacora` int(11) NOT NULL AUTO_INCREMENT,
  `idopcion` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idregistro` varchar(5) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechayhora` datetime DEFAULT NULL,
  `Accion` enum('Agrego','Modifico','Elimino') COLLATE utf8_spanish_ci DEFAULT NULL,
  `Cambios` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idbitacora`),
  KEY `fk_fk_opciones_bitacora` (`idopcion`),
  KEY `fk_fk_usuarios_bitacora` (`idusuario`),
  CONSTRAINT `fk_fk_opciones_bitacora` FOREIGN KEY (`idopcion`) REFERENCES `opciones` (`idopcion`),
  CONSTRAINT `fk_fk_usuarios_bitacora` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.bitacora: ~102 rows (aproximadamente)
DELETE FROM `bitacora`;
/*!40000 ALTER TABLE `bitacora` DISABLE KEYS */;
INSERT INTO `bitacora` (`idbitacora`, `idopcion`, `idusuario`, `idregistro`, `fechayhora`, `Accion`, `Cambios`) VALUES
	(24, 11, 0, '1', '2015-11-11 04:11:27', 'Agrego', NULL),
	(25, 11, 0, '1', '2015-11-11 04:11:31', 'Agrego', NULL),
	(26, 11, 0, '1', '2015-11-11 04:11:36', 'Agrego', NULL),
	(27, 11, 0, '1', '2015-11-11 04:12:03', 'Agrego', NULL),
	(28, 11, 0, '1', '2015-11-11 04:18:21', 'Agrego', NULL),
	(29, 11, 0, '1', '2015-11-11 04:18:26', 'Agrego', NULL),
	(32, 11, 0, '1', '2015-11-11 05:38:27', 'Agrego', NULL),
	(33, 11, 0, '1', '2015-11-11 05:51:50', 'Modifico', 'idusuario;login;vencimientocontrasena;nombre;apel,'),
	(34, 11, 0, '1', '2015-11-11 06:01:18', 'Elimino', NULL),
	(35, 12, 0, '3', '2015-11-11 21:32:43', 'Agrego', NULL),
	(36, 12, 0, '3', '2015-11-11 21:32:57', 'Agrego', NULL),
	(37, 12, 0, '3', '2015-11-11 21:33:03', 'Agrego', NULL),
	(38, 12, 0, '3', '2015-11-11 21:33:08', 'Agrego', NULL),
	(39, 11, 0, '1', '2015-11-11 21:33:52', 'Agrego', NULL),
	(40, 6, 0, '1', '2015-11-11 22:51:38', 'Agrego', NULL),
	(41, 6, 0, '1', '2015-11-11 22:51:50', 'Modifico', 'idcategoria;categoria;activocategoria;,'),
	(42, 6, 0, '1', '2015-11-11 22:58:09', 'Elimino', NULL),
	(43, 6, 0, '1', '2015-11-11 23:40:00', 'Agrego', NULL),
	(44, 6, 0, '2', '2015-11-11 23:40:14', 'Agrego', NULL),
	(45, 7, 0, '1', '2015-11-11 23:56:01', 'Agrego', NULL),
	(46, 7, 0, '1', '2015-11-12 00:00:02', 'Modifico', 'idsubcategoria;subcategoria;idcategoria;activosub,'),
	(47, 7, 0, '1', '2015-11-12 00:27:05', 'Elimino', NULL),
	(48, 7, 0, '1', '2015-11-12 00:29:55', 'Agrego', NULL),
	(49, 8, 0, '1', '2015-11-12 01:08:12', 'Agrego', NULL),
	(50, 8, 0, '2', '2015-11-12 01:08:28', 'Agrego', NULL),
	(51, 8, 0, '3', '2015-11-12 01:08:42', 'Agrego', NULL),
	(52, 8, 0, '3', '2015-11-12 01:09:29', 'Elimino', NULL),
	(53, 8, 0, '1', '2015-11-12 01:29:50', 'Modifico', 'idprioridad;prioridad;activoprioridad;,'),
	(54, 8, 0, '2', '2015-11-12 01:29:57', 'Modifico', 'idprioridad;prioridad;activoprioridad;,'),
	(55, 8, 0, '3', '2015-11-12 01:30:07', 'Agrego', NULL),
	(56, 9, 0, '0', '2015-11-12 01:30:32', 'Agrego', NULL),
	(57, 9, 0, '0', '2015-11-12 01:31:40', 'Elimino', NULL),
	(58, 9, 0, '1', '2015-11-12 01:32:16', 'Agrego', NULL),
	(59, 9, 0, '2', '2015-11-12 01:32:26', 'Agrego', NULL),
	(60, 9, 0, '3', '2015-11-12 01:33:40', 'Agrego', NULL),
	(61, 9, 0, '3', '2015-11-12 01:33:57', 'Modifico', 'iddepartamento;departamento;,'),
	(62, 10, 0, '1', '2015-11-12 02:03:57', 'Agrego', NULL),
	(63, 10, 0, '1', '2015-11-12 02:04:27', 'Elimino', NULL),
	(64, 10, 0, '1', '2015-11-12 02:04:52', 'Agrego', NULL),
	(65, 10, 0, '2', '2015-11-12 02:06:11', 'Agrego', NULL),
	(66, 10, 0, '1', '2015-11-12 02:06:16', 'Elimino', NULL),
	(67, 10, 0, '3', '2015-11-12 02:06:37', 'Agrego', NULL),
	(68, 10, 0, '3', '2015-11-12 02:07:36', 'Modifico', 'idmunicipio;municipio;iddepartamento;,'),
	(69, 5, 0, '1', '2015-11-12 02:46:48', 'Agrego', NULL),
	(70, 5, 0, '2', '2015-11-12 02:47:45', 'Agrego', NULL),
	(71, 5, 0, '3', '2015-11-12 02:48:03', 'Agrego', NULL),
	(72, 5, 0, '3', '2015-11-12 02:50:07', 'Modifico', 'idestado;estado;activoestado;,'),
	(73, 5, 0, '3', '2015-11-12 02:50:10', 'Elimino', NULL),
	(74, 4, 0, '1', '2015-11-12 03:25:03', 'Agrego', NULL),
	(75, 4, 0, '1', '2015-11-12 03:25:16', 'Modifico', 'idsolicitante;solicitante;telefonosolicitante;dir,'),
	(76, 4, 0, '1', '2015-11-12 03:25:25', 'Elimino', NULL),
	(77, 4, 0, '1', '2015-11-13 00:06:30', 'Agrego', NULL),
	(78, 4, 0, '2', '2015-11-13 00:08:00', 'Agrego', NULL),
	(79, 2, 0, '1', '2015-11-13 00:52:13', 'Agrego', NULL),
	(80, 2, 0, '2', '2015-11-13 00:54:39', 'Agrego', NULL),
	(81, 2, 0, '3', '2015-11-13 00:56:25', 'Agrego', NULL),
	(82, 2, 0, '4', '2015-11-13 00:59:23', 'Agrego', NULL),
	(83, 2, 0, '4', '2015-11-13 01:00:07', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(84, 2, 0, '2', '2015-11-13 01:37:05', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(85, 2, 0, '2', '2015-11-13 01:42:07', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(86, 2, 0, '2', '2015-11-13 01:44:28', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(87, 2, 0, '5', '2015-11-13 01:52:04', 'Agrego', NULL),
	(88, 2, 0, '5', '2015-11-13 01:52:28', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(89, 2, 0, '1', '2015-11-13 01:58:27', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(91, 3, 0, '1', '2015-11-13 02:40:12', 'Agrego', NULL),
	(92, 3, 0, '2', '2015-11-13 02:41:01', 'Agrego', NULL),
	(93, 3, 0, '3', '2015-11-13 02:51:01', 'Agrego', NULL),
	(94, 3, 0, '4', '2015-11-13 02:52:44', 'Agrego', NULL),
	(95, 3, 0, '5', '2015-11-13 02:55:23', 'Agrego', NULL),
	(96, 3, 0, '6', '2015-11-13 03:02:00', 'Agrego', NULL),
	(97, 3, 0, '7', '2015-11-13 03:02:16', 'Agrego', NULL),
	(98, 3, 0, '8', '2015-11-13 03:02:32', 'Agrego', NULL),
	(99, 2, 0, '5', '2015-11-13 03:02:43', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(100, 3, 0, '9', '2015-11-13 03:12:58', 'Agrego', NULL),
	(101, 3, 0, '10', '2015-11-13 03:15:10', 'Agrego', NULL),
	(102, 3, 0, '11', '2015-11-13 03:15:20', 'Agrego', NULL),
	(103, 3, 0, '12', '2015-11-13 03:15:27', 'Agrego', NULL),
	(104, 3, 0, '13', '2015-11-13 03:15:35', 'Agrego', NULL),
	(105, 2, 0, '2', '2015-11-13 03:28:19', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(106, 2, 0, '3', '2015-11-13 03:30:08', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(107, 2, 0, '3', '2015-11-13 03:30:18', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(108, 2, 0, '3', '2015-11-13 03:30:49', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(109, 2, 0, '6', '2015-11-13 03:36:46', 'Agrego', NULL),
	(110, 3, 0, '14', '2015-11-13 03:36:59', 'Agrego', NULL),
	(111, 2, 0, '6', '2015-11-13 03:37:08', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(112, 2, 0, '6', '2015-11-13 03:37:26', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(113, 3, 0, '15', '2015-11-13 03:39:37', 'Agrego', NULL),
	(114, 3, 0, '16', '2015-11-13 04:15:28', 'Agrego', NULL),
	(115, 3, 0, '17', '2015-11-13 04:15:33', 'Agrego', NULL),
	(116, 3, 0, '18', '2015-11-13 04:15:42', 'Agrego', NULL),
	(117, 3, 0, '19', '2015-11-13 04:15:56', 'Agrego', NULL),
	(118, 2, 0, '2', '2015-11-13 04:16:07', 'Modifico', 'idincidente;asunto;idsolicitante;descripcion;idsu,'),
	(119, 11, 0, '1', '2015-11-13 16:35:35', 'Modifico', 'idusuario;login;vencimientocontrasena;nombre;apel,'),
	(120, 12, 0, '4', '2015-11-13 21:17:22', 'Agrego', NULL),
	(121, 12, 0, '4', '2015-11-13 21:17:45', 'Agrego', NULL),
	(122, 12, 0, '4', '2015-11-13 21:17:50', 'Agrego', NULL),
	(123, 12, 0, '4', '2015-11-13 21:17:55', 'Agrego', NULL),
	(124, 12, 0, '4', '2015-11-13 21:18:03', 'Agrego', NULL),
	(125, 12, 0, '22', '2015-11-13 21:18:11', 'Elimino', NULL),
	(126, 12, 0, '4', '2015-11-13 21:18:18', 'Agrego', NULL),
	(127, 11, 0, '1', '2015-11-13 21:18:45', 'Modifico', 'idusuario;login;vencimientocontrasena;nombre;apel,'),
	(128, 7, 0, '2', '2015-11-14 01:03:59', 'Agrego', NULL);
/*!40000 ALTER TABLE `bitacora` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `idcategoria` int(11) NOT NULL,
  `categoria` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activocategoria` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.categorias: ~2 rows (aproximadamente)
DELETE FROM `categorias`;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` (`idcategoria`, `categoria`, `activocategoria`) VALUES
	(1, 'Soporte', 'Activo'),
	(2, 'Reclamos', 'Activo');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.comentariosincidente
CREATE TABLE IF NOT EXISTS `comentariosincidente` (
  `idcomentario` int(11) NOT NULL,
  `idincidente` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `comentario` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechahoracomentario` datetime DEFAULT NULL,
  PRIMARY KEY (`idcomentario`),
  KEY `fk_fk_incidentes_comentariosincidentes` (`idincidente`),
  KEY `fk_fk_usuarios_comentariosincidente` (`idusuario`),
  CONSTRAINT `fk_fk_incidentes_comentariosincidentes` FOREIGN KEY (`idincidente`) REFERENCES `incidentes` (`idincidente`),
  CONSTRAINT `fk_fk_usuarios_comentariosincidente` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.comentariosincidente: ~17 rows (aproximadamente)
DELETE FROM `comentariosincidente`;
/*!40000 ALTER TABLE `comentariosincidente` DISABLE KEYS */;
INSERT INTO `comentariosincidente` (`idcomentario`, `idincidente`, `idusuario`, `comentario`, `fechahoracomentario`) VALUES
	(1, 2, 0, 'Esto es un <strong>comentario</strong>, para verificar el funcionamiento', '2015-11-13 02:40:12'),
	(2, 2, 0, 'Verificar el <span style="color: #ff0000;">problema</span> de los <span style="font-size: 20px;">usuarios por favor</span>', '2015-11-13 02:41:01'),
	(3, 3, 0, 'Esto es un comentario', '2015-11-13 02:51:01'),
	(4, 2, 0, 'ver el problema por favor', '2015-11-13 02:52:44'),
	(5, 2, 0, 'ADSFADF', '2015-11-13 02:55:23'),
	(6, 5, 0, 'El problema es aislado con los routers marca next', '2015-11-13 03:02:00'),
	(7, 5, 0, 'Favor Verificar conel proveedor', '2015-11-13 03:02:17'),
	(8, 5, 0, 'posible problema en el transporte', '2015-11-13 03:02:33'),
	(9, 5, 0, 'verificar nuevamente', '2015-11-13 03:12:59'),
	(10, 4, 0, 'esto es una prueba', '2015-11-13 03:15:10'),
	(11, 4, 0, 'esta es otra', '2015-11-13 03:15:20'),
	(12, 4, 0, 'verificar nuevamente', '2015-11-13 03:15:27'),
	(13, 4, 0, 'tener en cuenta lo siguiente', '2015-11-13 03:15:36'),
	(14, 6, 0, 'Verificar nuevos eventos', '2015-11-13 03:37:00'),
	(15, 6, 0, 'asdfasdf', '2015-11-13 03:39:38'),
	(16, 2, 0, 'sdfasdfasdf', '2015-11-13 04:15:28'),
	(17, 2, 0, 'sdfasdfasdf', '2015-11-13 04:15:33'),
	(18, 2, 0, 'asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfsadfasdfasdf', '2015-11-13 04:15:42'),
	(19, 2, 0, 'asdfasdfasdf', '2015-11-13 04:15:57');
/*!40000 ALTER TABLE `comentariosincidente` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.departamentos
CREATE TABLE IF NOT EXISTS `departamentos` (
  `iddepartamento` int(11) NOT NULL,
  `departamento` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`iddepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.departamentos: ~2 rows (aproximadamente)
DELETE FROM `departamentos`;
/*!40000 ALTER TABLE `departamentos` DISABLE KEYS */;
INSERT INTO `departamentos` (`iddepartamento`, `departamento`) VALUES
	(1, 'Guatemala'),
	(2, 'Quiche'),
	(3, 'Huehuetenango');
/*!40000 ALTER TABLE `departamentos` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.estados
CREATE TABLE IF NOT EXISTS `estados` (
  `idestado` int(11) NOT NULL,
  `estado` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activoestado` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.estados: ~2 rows (aproximadamente)
DELETE FROM `estados`;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` (`idestado`, `estado`, `activoestado`) VALUES
	(1, 'Abierto', 'Activo'),
	(2, 'Cerrado', 'Activo');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.incidentes
CREATE TABLE IF NOT EXISTS `incidentes` (
  `idincidente` int(11) NOT NULL,
  `idsubcategoria` int(11) NOT NULL,
  `idestado` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idsolicitante` int(11) NOT NULL,
  `idprioridad` int(11) NOT NULL,
  `asunto` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechahoraincidente` datetime DEFAULT NULL,
  `fechahoracierre` datetime DEFAULT NULL,
  PRIMARY KEY (`idincidente`),
  KEY `fk_fk_estados_incidentes` (`idestado`),
  KEY `fk_fk_prioridad_incidentes` (`idprioridad`),
  KEY `fk_fk_solicitante_incidentes` (`idsolicitante`),
  KEY `fk_fk_subcategorias_incidentes` (`idsubcategoria`),
  KEY `fk_fk_usuarios_incidentes` (`idusuario`),
  CONSTRAINT `fk_fk_estados_incidentes` FOREIGN KEY (`idestado`) REFERENCES `estados` (`idestado`),
  CONSTRAINT `fk_fk_prioridad_incidentes` FOREIGN KEY (`idprioridad`) REFERENCES `prioridades` (`idprioridad`),
  CONSTRAINT `fk_fk_solicitante_incidentes` FOREIGN KEY (`idsolicitante`) REFERENCES `solicitantes` (`idsolicitante`),
  CONSTRAINT `fk_fk_subcategorias_incidentes` FOREIGN KEY (`idsubcategoria`) REFERENCES `subcategorias` (`idsubcategoria`),
  CONSTRAINT `fk_fk_usuarios_incidentes` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.incidentes: ~6 rows (aproximadamente)
DELETE FROM `incidentes`;
/*!40000 ALTER TABLE `incidentes` DISABLE KEYS */;
INSERT INTO `incidentes` (`idincidente`, `idsubcategoria`, `idestado`, `idusuario`, `idsolicitante`, `idprioridad`, `asunto`, `descripcion`, `fechahoraincidente`, `fechahoracierre`) VALUES
	(1, 1, 2, 0, 1, 1, 'Prueba', 'Este es un incidente de prueba para verificar el funcionamiento del sistema', '2015-11-13 01:58:28', '2015-11-13 01:58:28'),
	(2, 1, 2, 0, 1, 3, 'Edicion Prueba 2', 'Esta es otra prueba de incidentes. Aki Tambien se agrego. MAS VARIANTES', '2015-11-13 04:16:08', '2015-11-13 04:16:08'),
	(3, 1, 1, 0, 2, 2, 'Prueba 2. edicion', 'ASDFASDFASDF', '2015-11-13 03:30:49', NULL),
	(4, 1, 1, 0, 1, 1, 'Prueba 4', 'Esta es otra prueba del funcionamiento.  Otro agregado', '2015-11-13 01:00:08', NULL),
	(5, 1, 1, 0, 2, 1, 'Verificar Router', 'Se reporto una falla en los routers vendidos a Angelo Galindo, el problema se debe verificar', '2015-11-13 03:02:43', NULL),
	(6, 1, 2, 0, 1, 1, 'problemas en impresoras', 'esto es una prueba del funcionamento', '2015-11-13 03:37:27', '2015-11-13 03:37:27');
/*!40000 ALTER TABLE `incidentes` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.municipios
CREATE TABLE IF NOT EXISTS `municipios` (
  `idmunicipio` int(11) NOT NULL,
  `iddepartamento` int(11) NOT NULL,
  `municipio` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idmunicipio`),
  KEY `fk_fk_departamento_municipios` (`iddepartamento`),
  CONSTRAINT `fk_fk_departamento_municipios` FOREIGN KEY (`iddepartamento`) REFERENCES `departamentos` (`iddepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.municipios: ~2 rows (aproximadamente)
DELETE FROM `municipios`;
/*!40000 ALTER TABLE `municipios` DISABLE KEYS */;
INSERT INTO `municipios` (`idmunicipio`, `iddepartamento`, `municipio`) VALUES
	(2, 1, 'Ciudad de Guatemala'),
	(3, 1, 'Sta. Catarina Pinula');
/*!40000 ALTER TABLE `municipios` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.opciones
CREATE TABLE IF NOT EXISTS `opciones` (
  `idopcion` int(11) NOT NULL,
  `opcion` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `formularioopcion` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `enmenu` enum('Si','No') COLLATE utf8_spanish_ci DEFAULT NULL,
  `grupo` enum('Operaciones','Catalogos') COLLATE utf8_spanish_ci DEFAULT NULL,
  `activoopcion` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idopcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.opciones: ~12 rows (aproximadamente)
DELETE FROM `opciones`;
/*!40000 ALTER TABLE `opciones` DISABLE KEYS */;
INSERT INTO `opciones` (`idopcion`, `opcion`, `formularioopcion`, `enmenu`, `grupo`, `activoopcion`) VALUES
	(1, 'Incidentes', 'ListadoIncidentes.aspx', 'Si', 'Operaciones', 'Activo'),
	(2, 'Crear Incidente', 'Incidentes.aspx', 'No', 'Operaciones', 'Activo'),
	(3, 'Comentar Incidentes', 'Incidentes.aspx', 'No', 'Operaciones', 'Activo'),
	(4, 'Solicitantes', 'Solicitantes.aspx', 'Si', 'Catalogos', 'Activo'),
	(5, 'Estados', 'Estados.aspx', 'Si', 'Catalogos', 'Activo'),
	(6, 'Categorias', 'Categorias.aspx', 'Si', 'Catalogos', 'Activo'),
	(7, 'SubCategorias', 'SubCategorias.aspx', 'Si', 'Catalogos', 'Activo'),
	(8, 'Prioridades', 'Prioridades.aspx', 'Si', 'Catalogos', 'Activo'),
	(9, 'Departamentos', 'Departamentos.aspx', 'Si', 'Catalogos', 'Activo'),
	(10, 'Municipios', 'Municipios.aspx', 'Si', 'Catalogos', 'Activo'),
	(11, 'Usuarios', 'Usuarios.aspx', 'Si', 'Catalogos', 'Activo'),
	(12, 'Roles de Usuario', 'RolesDeUsuario.aspx', 'Si', 'Catalogos', 'Activo');
/*!40000 ALTER TABLE `opciones` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.opcionesporrol
CREATE TABLE IF NOT EXISTS `opcionesporrol` (
  `idopcionporrol` int(11) NOT NULL AUTO_INCREMENT,
  `idrol` int(11) NOT NULL,
  `idopcion` int(11) NOT NULL,
  PRIMARY KEY (`idopcionporrol`),
  UNIQUE KEY `idrol_idopcion` (`idrol`,`idopcion`),
  KEY `fk_fk_opciones_opcionporrol` (`idopcion`),
  KEY `fk_fk_roles_opcionesporrol` (`idrol`),
  CONSTRAINT `fk_fk_opciones_opcionporrol` FOREIGN KEY (`idopcion`) REFERENCES `opciones` (`idopcion`),
  CONSTRAINT `fk_fk_roles_opcionesporrol` FOREIGN KEY (`idrol`) REFERENCES `roles` (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.opcionesporrol: ~13 rows (aproximadamente)
DELETE FROM `opcionesporrol`;
/*!40000 ALTER TABLE `opcionesporrol` DISABLE KEYS */;
INSERT INTO `opcionesporrol` (`idopcionporrol`, `idrol`, `idopcion`) VALUES
	(10, 1, 1),
	(11, 1, 2),
	(12, 1, 3),
	(13, 1, 4),
	(14, 1, 9),
	(15, 1, 10),
	(16, 3, 1),
	(17, 3, 2),
	(18, 3, 3),
	(20, 4, 1),
	(19, 4, 2),
	(21, 4, 4),
	(23, 4, 7);
/*!40000 ALTER TABLE `opcionesporrol` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.prioridades
CREATE TABLE IF NOT EXISTS `prioridades` (
  `idprioridad` int(11) NOT NULL,
  `prioridad` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activoprioridad` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idprioridad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.prioridades: ~2 rows (aproximadamente)
DELETE FROM `prioridades`;
/*!40000 ALTER TABLE `prioridades` DISABLE KEYS */;
INSERT INTO `prioridades` (`idprioridad`, `prioridad`, `activoprioridad`) VALUES
	(1, 'Alta', 'Activo'),
	(2, 'Media', 'Activo'),
	(3, 'Baja', 'Activo');
/*!40000 ALTER TABLE `prioridades` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activorol` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.roles: ~4 rows (aproximadamente)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`idrol`, `rol`, `activorol`) VALUES
	(0, 'Super Usuario', 'Activo'),
	(1, 'Admin Ventas', 'Activo'),
	(2, 'Usuario Normal', 'Activo'),
	(3, 'Usuario Limitado', 'Activo'),
	(4, 'Jefe de Ventas', 'Activo');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.solicitantes
CREATE TABLE IF NOT EXISTS `solicitantes` (
  `idsolicitante` int(11) NOT NULL,
  `idmunicipio` int(11) NOT NULL,
  `solicitante` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefonosolicitante` varchar(8) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccionsolicitante` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activosolicitante` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idsolicitante`),
  KEY `fk_fk_municipios_incidentes` (`idmunicipio`),
  CONSTRAINT `fk_fk_municipios_incidentes` FOREIGN KEY (`idmunicipio`) REFERENCES `municipios` (`idmunicipio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.solicitantes: ~2 rows (aproximadamente)
DELETE FROM `solicitantes`;
/*!40000 ALTER TABLE `solicitantes` DISABLE KEYS */;
INSERT INTO `solicitantes` (`idsolicitante`, `idmunicipio`, `solicitante`, `telefonosolicitante`, `direccionsolicitante`, `activosolicitante`) VALUES
	(1, 2, 'Erick Brol Natareno', '43558978', '4ta Calle 3-45', 'Activo'),
	(2, 2, 'Angelo Galindo', '43294334', '8a Av. 22-28 z12', 'Activo');
/*!40000 ALTER TABLE `solicitantes` ENABLE KEYS */;


-- Volcando estructura para procedimiento incidentes.SP_ValidaUsuario
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ValidaUsuario`(IN `Login` VARCHAR(10), IN `Contrasena` VARCHAR(20), OUT `IDUsuario` INT, OUT `Usuario` VARCHAR(50), OUT `Mensaje` VARCHAR(50))
    SQL SECURITY INVOKER
    COMMENT 'Verifica que el usuario y contraseña ingresada sean los correctos'
BEGIN
	
	SELECT 
		u.idusuario, CONCAT( u.nombre, ' ' , u.apellido ) into IDUsuario,Usuario
	FROM usuarios u
	WHERE u.login=Login AND u.contrasena=SHA1(Contrasena);
	
	IF IDUsuario IS NULL THEN
		SET Mensaje = 'Usuario y Contraseña incorrectos';
	END IF;
		
END//
DELIMITER ;


-- Volcando estructura para tabla incidentes.subcategorias
CREATE TABLE IF NOT EXISTS `subcategorias` (
  `idsubcategoria` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `subcategoria` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activosubcategoria` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idsubcategoria`),
  KEY `fk_fk_categorias_subcategorias` (`idcategoria`),
  CONSTRAINT `fk_fk_categorias_subcategorias` FOREIGN KEY (`idcategoria`) REFERENCES `categorias` (`idcategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.subcategorias: ~1 rows (aproximadamente)
DELETE FROM `subcategorias`;
/*!40000 ALTER TABLE `subcategorias` DISABLE KEYS */;
INSERT INTO `subcategorias` (`idsubcategoria`, `idcategoria`, `subcategoria`, `activosubcategoria`) VALUES
	(1, 1, 'Routers Nexxt', 'Activo'),
	(2, 2, 'Mala calidad', 'Activo');
/*!40000 ALTER TABLE `subcategorias` ENABLE KEYS */;


-- Volcando estructura para tabla incidentes.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuario` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `login` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `contrasena` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `vencimientocontrasena` date DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `activousuario` enum('Activo','Inactivo') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_fk_roles_usuario` (`idrol`),
  CONSTRAINT `fk_fk_roles_usuario` FOREIGN KEY (`idrol`) REFERENCES `roles` (`idrol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla incidentes.usuarios: ~1 rows (aproximadamente)
DELETE FROM `usuarios`;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`idusuario`, `idrol`, `login`, `contrasena`, `vencimientocontrasena`, `nombre`, `apellido`, `activousuario`) VALUES
	(0, 0, 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2030-12-31', 'Super', 'Usuario', 'Activo'),
	(1, 4, 'ebrol', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '2016-02-11', 'Erick', 'Brol', 'Activo');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;


-- Volcando estructura para vista incidentes.view_bitacora
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_bitacora` (
	`idregistro` VARCHAR(5) NULL COLLATE 'utf8_spanish_ci',
	`fechayhora` DATETIME NULL,
	`Accion` ENUM('Agrego','Modifico','Elimino') NULL COLLATE 'utf8_spanish_ci',
	`opcion` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci',
	`login` VARCHAR(10) NULL COLLATE 'utf8_spanish_ci',
	`nombre` VARCHAR(101) NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_comentarios
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_comentarios` (
	`idcomentario` INT(11) NOT NULL,
	`idincidente` INT(11) NOT NULL,
	`comentario` VARCHAR(300) NULL COLLATE 'utf8_spanish_ci',
	`fechahora` DATETIME NULL,
	`usuario` VARCHAR(101) NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_listadoincidentes
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_listadoincidentes` (
	`idincidente` INT(11) NOT NULL,
	`idsubcategoria` INT(11) NOT NULL,
	`idestado` INT(11) NOT NULL,
	`idusuario` INT(11) NOT NULL,
	`idsolicitante` INT(11) NOT NULL,
	`idprioridad` INT(11) NOT NULL,
	`asunto` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci',
	`descripcion` VARCHAR(300) NULL COLLATE 'utf8_spanish_ci',
	`fechahoraincidente` DATETIME NULL,
	`fechahoracierre` DATETIME NULL,
	`solicitante` VARCHAR(20) NULL COLLATE 'utf8_spanish_ci',
	`estado` VARCHAR(15) NULL COLLATE 'utf8_spanish_ci',
	`login` VARCHAR(10) NULL COLLATE 'utf8_spanish_ci',
	`idcategoria` INT(11) NOT NULL
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_municipios
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_municipios` (
	`idmunicipio` INT(11) NOT NULL,
	`municipio` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci',
	`departamento` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_opcionesporrol
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_opcionesporrol` (
	`idopcionporrol` INT(11) NOT NULL,
	`idrol` INT(11) NOT NULL,
	`opcion` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci',
	`grupo` ENUM('Operaciones','Catalogos') NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_solicitantes
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_solicitantes` (
	`idsolicitante` INT(11) NOT NULL,
	`solicitante` VARCHAR(20) NULL COLLATE 'utf8_spanish_ci',
	`telefonosolicitante` VARCHAR(8) NULL COLLATE 'utf8_spanish_ci',
	`direccionsolicitante` VARCHAR(20) NULL COLLATE 'utf8_spanish_ci',
	`activosolicitante` ENUM('Activo','Inactivo') NULL COLLATE 'utf8_spanish_ci',
	`municipio` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_subcategorias
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_subcategorias` (
	`idsubcategoria` INT(11) NOT NULL,
	`subcategoria` VARCHAR(20) NULL COLLATE 'utf8_spanish_ci',
	`categoria` VARCHAR(20) NULL COLLATE 'utf8_spanish_ci',
	`activosubcategoria` ENUM('Activo','Inactivo') NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_usuarios
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_usuarios` (
	`idusuario` INT(11) NOT NULL,
	`login` VARCHAR(10) NULL COLLATE 'utf8_spanish_ci',
	`nombre` VARCHAR(50) NULL COLLATE 'utf8_spanish_ci',
	`apellido` VARCHAR(50) NULL COLLATE 'utf8_spanish_ci',
	`rol` VARCHAR(20) NULL COLLATE 'utf8_spanish_ci',
	`activousuario` ENUM('Activo','Inactivo') NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_verificarpermisos
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `view_verificarpermisos` (
	`idusuario` BIGINT(20) NOT NULL,
	`idopcion` INT(11) NOT NULL,
	`formularioopcion` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci',
	`opcion` VARCHAR(30) NULL COLLATE 'utf8_spanish_ci',
	`grupo` BIGINT(20) UNSIGNED NULL,
	`enmenu` BIGINT(20) UNSIGNED NULL,
	`ruta` VARCHAR(44) NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista incidentes.view_bitacora
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_bitacora`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_bitacora` AS SELECT 
	b.idregistro,
	b.fechayhora,
	b.Accion,
	o.opcion,
	u.login,
	CONCAT(u.nombre,' ',u.apellido) nombre
FROM 
	bitacora b 
	INNER JOIN opciones o ON b.idopcion=o.idopcion
	INNER JOIN usuarios u ON b.idusuario=u.idusuario ;


-- Volcando estructura para vista incidentes.view_comentarios
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_comentarios`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_comentarios` AS SELECT 
	c.idcomentario,
	c.idincidente,
	c.comentario,
	c.fechahoracomentario fechahora,
	CONCAT(u.nombre, ' ',u.apellido) as usuario
FROM 
	comentariosincidente c
	INNER JOIN usuarios u ON c.idusuario=u.idusuario ;


-- Volcando estructura para vista incidentes.view_listadoincidentes
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_listadoincidentes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_listadoincidentes` AS SELECT  
	i.idincidente,
	i.idsubcategoria,
	i.idestado,
	i.idusuario,
	i.idsolicitante,
	i.idprioridad,
	i.asunto,
	i.descripcion,
	i.fechahoraincidente,
	i.fechahoracierre,
	s.solicitante,
	e.estado,
	u.login,
	cat.idcategoria
FROM 
	incidentes  i
	INNER JOIN solicitantes s ON i.idsolicitante=s.idsolicitante
	INNER JOIN estados e ON i.idestado=e.idestado
	INNER JOIN usuarios u ON i.idusuario=u.idusuario 
	INNER JOIN subcategorias sub ON sub.idsubcategoria=i.idsubcategoria
	INNER JOIN categorias cat ON sub.idcategoria=cat.idcategoria ;


-- Volcando estructura para vista incidentes.view_municipios
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_municipios`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_municipios` AS SELECT 
	m.idmunicipio,
	m.municipio,
	d.departamento
FROM municipios m
INNER JOIN departamentos d ON m.iddepartamento=d.iddepartamento ;


-- Volcando estructura para vista incidentes.view_opcionesporrol
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_opcionesporrol`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_opcionesporrol` AS SELECT 
	opcionesporrol.idopcionporrol,
	roles.idrol,
	opciones.opcion,
	opciones.grupo
FROM opciones
INNER JOIN opcionesporrol ON opciones.idopcion=opcionesporrol.idopcion
INNER JOIN roles ON opcionesporrol.idrol=roles.idrol ;


-- Volcando estructura para vista incidentes.view_solicitantes
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_solicitantes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_solicitantes` AS SELECT
	s.idsolicitante,
	s.solicitante,
	s.telefonosolicitante,
	s.direccionsolicitante,
	s.activosolicitante,
	m.municipio
FROM solicitantes s
INNER JOIN municipios m ON s.idmunicipio=m.idmunicipio ;


-- Volcando estructura para vista incidentes.view_subcategorias
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_subcategorias`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_subcategorias` AS SELECT 
	s.idsubcategoria,
	s.subcategoria,
	c.categoria,
	s.activosubcategoria
FROM subcategorias s
INNER JOIn categorias c ON s.idcategoria=c.idcategoria ;


-- Volcando estructura para vista incidentes.view_usuarios
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_usuarios`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_usuarios` AS SELECT 
	u.idusuario,
	u.login,
	u.nombre,
	u.apellido,
	r.rol,
	u.activousuario
FROM usuarios u
INNER JOIN roles r ON u.idrol=r.idrol
WHERE 
	u.idusuario>0 ;


-- Volcando estructura para vista incidentes.view_verificarpermisos
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `view_verificarpermisos`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_verificarpermisos` AS select 
	u.idusuario,
	op.idopcion,
	op.formularioopcion,
	op.opcion,
	CONVERT(op.grupo,unsigned Int) grupo,
	CONVERT(op.enmenu,unsigned Int) enmenu,
	concat('~/',grupo , '/',formularioopcion) as ruta
from 
	usuarios u
	INNER JOIN roles r ON u.idrol=r.idrol
	INNER JOIN opcionesporrol opr ON r.idrol=opr.idrol
	INNER JOIN opciones op ON opr.idopcion=op.idopcion
union all
select 
	0 as idusuario,
	op.idopcion,
	op.formularioopcion,
	op.opcion,
	CONVERT(op.enmenu,unsigned Int) enmenu,
CONVERT(op.grupo,unsigned Int) grupo,
	concat('~/',grupo , '/',formularioopcion) as ruta
from 
	opciones op ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
