﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;

namespace CAD
{
    public class CAD_Mysql
    {
        public delegate void OnErrorDelegado(string DescripcionError);
        public event OnErrorDelegado onError;
        
        private string mCN;

        public string MensajeError;

        public CAD_Mysql() {
            mCN = @"server=localhost;user id=UserIncidentes;password=!nc1d3nt3$;persistsecurityinfo=True;database=incidentes";
        }


        //Inserta un nuevo registro
        public bool IngresarDatosNuevos(string Tabla, MySqlParameterCollection Parametros, Int16 IDUSuario, Nullable<Int16> IDOpcion = null,bool RegistraBitacora = true)
        {
            bool logResp = true;

            MySqlConnection cn = new MySqlConnection(mCN);
            string NombresCampos = "";
            string ValoresCampos = "";

            cn.Open();
            MySqlCommand cmd = cn.CreateCommand();
            MySqlTransaction Transaccion = cn.BeginTransaction();
            cmd.Transaction = Transaccion;
            cmd.CommandType = CommandType.Text;

            try
            {
                if (RegistraBitacora)
                {
                    //Insertar en la bitacora
                    cmd.CommandText = String.Format("Insert into bitacora (idopcion,idusuario,idregistro,fechayhora,Accion) Values({0},{1},{2},NOW(),{3})", IDOpcion, IDUSuario,Parametros[0].Value, 1);
                    cmd.ExecuteNonQuery(); 
                }

                //Construye query para realizar insersion
                for (int i = 0; i <= Parametros.Count - 1; i++)
                {
                   
                    NombresCampos += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1);
                    ValoresCampos += Parametros[i].ParameterName;
                    
                    if (i < Parametros.Count - 1)
                    {
                        NombresCampos += ",";
                        ValoresCampos += ",";
                    }
                    cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                }

                cmd.CommandText = "Insert Into " + Tabla + "(" + NombresCampos + ") Values(" + ValoresCampos + ")";
                cmd.ExecuteNonQuery();
                Transaccion.Commit();

                logResp = true;
            }
            catch (Exception ex)
            {
                Transaccion.Rollback();
                LevantarError(ex);
                logResp = false;
            }

            if (cmd != null) {
                cmd.Dispose();
                Transaccion.Dispose();
                cn.Close();
                cn.Dispose();
                Parametros.Clear();
            }

            return logResp;

        }
        
        //Actualiza un registro seleccionado
        public bool ActualizarDatos(string Tabla, string NombrePK, int ValorPK, MySqlParameterCollection Parametros, Int16 IDUSuario, Nullable<Int16> IDOpcion = null,bool RegistraBitacora = true)
        {
            bool logRespuesta = true;
            string CamposModificados;

            MySqlConnection cn = new MySqlConnection(mCN);
            cn.Open();
            MySqlCommand cmd = new MySqlCommand();
            MySqlDataAdapter da = new MySqlDataAdapter();
            DataTable dtTabla = new DataTable();
            string Sql = "";
            string CamposSelect = "";
            string Cambios = "";
            int i = 0;

            for (i = 0; i <= Parametros.Count - 1; i++)
            {
                CamposSelect += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1);
                if (i < Parametros.Count - 1)
                {
                    CamposSelect += ",";
                }
            }
            Sql = "SELECT " + CamposSelect + " FROM " + Tabla + " WHERE " + NombrePK + "=" + ValorPK;

            da = new MySqlDataAdapter(Sql, cn);
            da.Fill(dtTabla);

            cmd.Connection = cn;
            MySqlTransaction Transaccion = cn.BeginTransaction();
            cmd.Transaction = Transaccion;
            cmd.CommandType = CommandType.Text;


            try
            {
                cmd.CommandText = "Update " + Tabla + " Set ";
                for (i = 0; i <= Parametros.Count - 1; i++)
                {
                    if (Information.IsDBNull(dtTabla.Rows[0].ItemArray[i]) || Information.IsDBNull(Parametros[i].Value))
                    {
                        if (dtTabla.Rows[0].ItemArray[i].ToString() != Parametros[i].Value.ToString())
                        {
                            Cambios += dtTabla.Columns[i].ColumnName + ";";

                            cmd.CommandText += dtTabla.Columns[i].ColumnName + "=" + Parametros[i].ParameterName + ",";
                            cmd.Parameters.Add(Parametros[i].ParameterName, Parametros[i].MySqlDbType).Value = Parametros[i].Value;
                        }
                    }
                    else
                    {
                        if (dtTabla.Rows[0].ItemArray[i] != Parametros[i].Value)
                        {
                            Cambios += dtTabla.Columns[i].ColumnName + ";";

                            cmd.CommandText += dtTabla.Columns[i].ColumnName + "=" + Parametros[i].ParameterName + ",";
                            cmd.Parameters.Add(Parametros[i].ParameterName, Parametros[i].MySqlDbType).Value = Parametros[i].Value;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Cambios))
                {
                    cmd.CommandText += " Where " + NombrePK + "=" + ValorPK;
                    cmd.CommandText = Strings.Replace(cmd.CommandText, ", Where ", " Where ");
                    cmd.ExecuteNonQuery();
                    if (RegistraBitacora)
                    {
                        cmd.CommandText = String.Format("Insert into bitacora (idopcion,idusuario,idregistro,fechayhora,Accion,Cambios) Values({0},{1},{2},NOW(),{3},'{4}')", IDOpcion, IDUSuario, Parametros[0].Value, 2, Strings.Left(Cambios, 49) + ",");                        
                        cmd.ExecuteNonQuery();
                    }
                }
                CamposModificados = Cambios;
                Transaccion.Commit();
                
                logRespuesta = true;
            }
            catch (Exception ex)
            {
                Transaccion.Rollback();
                LevantarError(ex);

                logRespuesta = false;
            }

            if (cmd != null) {
                cmd.Dispose();
                Transaccion.Dispose();
                cn.Close();
                cn.Dispose();
                Parametros.Clear();
            }

            return logRespuesta;
        }

        //Elimina un registro determinado de la tabla seleccionada.
        public bool EliminarDatos(string Tabla, string Campo, Int32 PK, Int16 IDUSuario, Nullable<Int16> IDOpcion, bool RegistraBitacora = true)
        {
            bool logRespuesta = true;

            MySqlConnection cn = new MySqlConnection(mCN);

            cn.Open();
            MySqlCommand cmd = cn.CreateCommand();
            MySqlTransaction Transaccion = cn.BeginTransaction();
            cmd.Transaction = Transaccion;
            cmd.CommandType = CommandType.Text;

            try
            {
                if (RegistraBitacora)
                {
                    //Insertar en la bitacora
                    cmd.CommandText = String.Format("Insert into bitacora (idopcion,idusuario,idregistro,fechayhora,Accion) Values({0},{1},{2},NOW(),{3})", IDOpcion, IDUSuario, PK, 3); 
                    cmd.ExecuteNonQuery();
                }
                //Construye query para realizar eliminacion
                cmd.CommandText = "Delete from " + Tabla + " Where " + Campo + "=" + PK;
                cmd.ExecuteNonQuery();
                Transaccion.Commit();

                logRespuesta = true;
            }
            catch (Exception ex)
            {
                Transaccion.Rollback();

                LevantarError(ex);
                logRespuesta = false;
            }

            if (cmd != null) {
                cmd.Dispose();
                Transaccion.Dispose();
                cn.Close();
                cn.Dispose();
            }

            return logRespuesta;
        }

        //Retorna un conjunto de datos de una tabla o vista especificada
        public DataTable RetornarDatos(string Tabla, string Campos, MySqlParameterCollection Parametros, string CondicionWhere)
        {
            DataTable dtResultado = new DataTable();
            MySqlDataAdapter daResultado = default(MySqlDataAdapter);

            try
            {
                MySqlConnection cn = new MySqlConnection(mCN);
                cn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "SELECT " + Campos + "  FROM " + Tabla;

                if (Parametros != null)
                {
                    if (Parametros.Count > 0)
                    {
                        cmd.CommandText += " Where " + CondicionWhere;
                        for (int i = 0; i <= Parametros.Count - 1; i++)
                        {
                            cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                        }
                    }
                }

                daResultado = new MySqlDataAdapter(cmd);
                daResultado.Fill(dtResultado);

                daResultado.Dispose();
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }
            return dtResultado;
        }

        //Retorna el valor maximo de un campo en una tabla, se utiliza con campos numericos
        public int Max(string Tabla, string Campo, MySqlParameterCollection Parametros = null)
        {
            int Resultado = 0;
            try
            {
                MySqlConnection cn = new MySqlConnection(mCN);
                cn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select ifnull(max(" + Campo + "),0) + 1 From " + Tabla;

                if (Parametros != null)
                {
                    if (Parametros.Count > 0)
                    {
                        cmd.CommandText += " Where ";
                        for (int i = 0; i <= Parametros.Count - 1; i++)
                        {
                            cmd.CommandText += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1) + "=" + Parametros[i].ParameterName;
                            if (i < Parametros.Count - 1)
                            {
                                cmd.CommandText += " and ";
                            }
                            cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                        }
                    }
                }

                Resultado = Convert.ToInt32(cmd.ExecuteScalar());
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }
            return Resultado;
        }

        //Retorna el valor minimo de un campo en una tabla, se utiliza con campos numericos
        public int Min(string Tabla, string Campo, MySqlParameterCollection Parametros = null)
        {
            int Resultado = 0;
            try
            {
                MySqlConnection cn = new MySqlConnection(mCN);
                cn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select isnull(min(" + Campo + "),0)-1 From " + Tabla;

                if (Parametros != null)
                {
                    if (Parametros.Count > 0)
                    {
                        cmd.CommandText += " Where ";
                        for (int i = 0; i <= Parametros.Count - 1; i++)
                        {
                            cmd.CommandText += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1) + "=" + Parametros[i].ParameterName;
                            if (i < Parametros.Count - 1)
                            {
                                cmd.CommandText += " and ";
                            }
                            cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                        }
                    }
                }

                Resultado = Convert.ToInt32(cmd.ExecuteScalar());
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }
            return Resultado;
        }

        //Retorna el valor de un campo en una tabla, retorna cualquier campo a string
        public object RetornarValor(string Tabla, string Campo, MySqlParameterCollection Parametros)
        {
            string Resultado = "";
            try
            {
                MySqlConnection cn = new MySqlConnection(mCN);
                cn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select " + Campo + " From " + Tabla;

                cmd.CommandText += " Where ";
                for (int i = 0; i <= Parametros.Count - 1; i++)
                {
                    cmd.CommandText += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1) + "=" + Parametros[i].ParameterName;
                    if (i < Parametros.Count - 1)
                    {
                        cmd.CommandText += " and ";
                    }
                    cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                }

                Resultado = Convert.ToString(cmd.ExecuteScalar());
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }
            return Resultado;
        }
        
        //Permite ejecutar un procedimiento almacenado, sin retorno de datos, opcionalmente retorna un parametro de salida. 
        public bool EjecutarSP(string NombreSP, ref MySqlParameterCollection Parametros, bool ParametrosDeSalida = false)
        {
            bool logRepuesta = true;
            MySqlCommand cmd = new MySqlCommand();
            MySqlConnection cn = new MySqlConnection(mCN);
            Int16 i = 0;
            cn.Open();

            try
            {
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = NombreSP;

                if (Parametros != null)
                {
                    for (i = 0; i <= Parametros.Count - 1; i++)
                    {
                        cmd.Parameters.Add(Parametros[i].ParameterName.ToString(), Parametros[i].MySqlDbType, Parametros[i].Size).Value = Parametros[i].Value;
                        cmd.Parameters[i].Direction = Parametros[i].Direction;
                    }
                }

                cmd.ExecuteNonQuery();
                if (ParametrosDeSalida)
                {
                    for (i = 0; i <= cmd.Parameters.Count - 1; i++)
                    {
                        Parametros[i].Value = cmd.Parameters[i].Value;
                    }
                }

                cn.Close();
                cmd.Dispose();
                logRepuesta = true;
            }
            catch (Exception ex)
            {
                LevantarError(ex);

                logRepuesta = false;
            }

            return logRepuesta;
        }

        //permite ejecutar un procedimiento almacenado, retornando los datos en data table. Opcionalmente retorna parametros de salida.
        public DataTable EjecutarSPConDatos(string NombreSP, ref MySqlParameterCollection Parametros, bool ParametrosDeSalida = false)
        {
            DataTable dtTabla = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter();
            MySqlConnection cn = new MySqlConnection(mCN);
            Int16 i = 0;
            cn.Open();
            MySqlCommand cmd = new MySqlCommand();

            try
            {
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = NombreSP;
                if (Parametros != null)
                {
                    for (i = 0; i <= Parametros.Count - 1; i++)
                    {
                        cmd.Parameters.Add(Parametros[i].ParameterName.ToString(), Parametros[i].MySqlDbType, Parametros[i].Size).Value = Parametros[i].Value;
                        cmd.Parameters[i].Direction = Parametros[i].Direction;
                    }
                }

                da = new MySqlDataAdapter(cmd);
                da.Fill(dtTabla);

                if (ParametrosDeSalida)
                {
                    for (i = 0; i <= cmd.Parameters.Count - 1; i++)
                    {
                        Parametros[i].Value = cmd.Parameters[i].Value;
                    }
                }

            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }

            cmd.Dispose();
            da.Dispose();

            return dtTabla;
        }
        
        //Ejecuta un comando de sql 
        public bool EjecutarSQL(string SQL, MySqlParameterCollection Parametros)
        {
            bool logRespuesta = true;
            MySqlConnection cn = new MySqlConnection(mCN);
            MySqlCommand cmd = new MySqlCommand();
            cn.Open();
            cmd = new MySqlCommand(SQL, cn);
            try
            {
                for (int i = 0; i <= Parametros.Count - 1; i++)
                {
                    cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                }
                cmd.ExecuteNonQuery();
                
                logRespuesta = true;
            }
            catch (Exception ex)
            {   
                LevantarError(ex);
                logRespuesta = false;
            }

            if (cmd != null)
            {
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
                Parametros.Clear();
            } 

            return logRespuesta;
        }

        //funcion que invoca evento de error
        public void LevantarError(Exception ex)
        {
            MensajeError = ex.Message;
            OnErrorDelegado EventoError = onError;

            if (EventoError != null)
            {
                EventoError(MensajeError);
            }
            
        }

    }
}
