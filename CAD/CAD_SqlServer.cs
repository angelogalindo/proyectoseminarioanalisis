﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace CAD
{
    public class CAD_SqlServer
    {
        public delegate void OnErrorDelegado(string DescripcionError);
        public event OnErrorDelegado onError;

        private string mCN;

        public string MensajeError;

        public CAD_SqlServer() {
            mCN = @"Data Source=AngeloPortatil;Initial Catalog=comser;User ID=SistemaComser;Password=comser**db;";
        }

        //Inserta un nuevo registro
        public bool IngresarDatosNuevos(ref string Tabla, ref SqlParameterCollection Parametros, Int16 IDUSuario, string IP, bool RegistraBitacora = true)
        {
            bool logResp = true;

            SqlConnection cn = new SqlConnection(mCN);
            string NombresCampos = "";
            string ValoresCampos = "";

            cn.Open();
            SqlCommand cmd = cn.CreateCommand();
            SqlTransaction Transaccion = cn.BeginTransaction();
            cmd.Transaction = Transaccion;
            cmd.CommandType = CommandType.Text;

            try
            {
                if (RegistraBitacora)
                {
                    //Insertar en la bitacora
                    cmd.CommandText = "Insert into M_BitacoraTransacciones (IDEmpresa,IDModulo,AccionBit,TablaBit,ValorLlavePrimaria,IDUsuario,Terminal) Values('A','" + Tabla + "'," + Parametros[0].Value + "," + IDUSuario + ",'" + IP + "')";
                    cmd.ExecuteNonQuery();
                }

                //Construye query para realizar insersion
                for (int i = 0; i <= Parametros.Count - 1; i++)
                {

                    NombresCampos += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1);
                    ValoresCampos += Parametros[i].ParameterName;

                    if (i < Parametros.Count - 1)
                    {
                        NombresCampos += ",";
                        ValoresCampos += ",";
                    }
                    cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                }

                cmd.CommandText = "Insert Into " + Tabla + "(" + NombresCampos + ") Values(" + ValoresCampos + ")";
                cmd.ExecuteNonQuery();
                Transaccion.Commit();

                logResp = true;
            }
            catch (Exception ex)
            {
                Transaccion.Rollback();
                LevantarError(ex);
                logResp = false;
            }

            if (cmd != null)
            {
                cmd.Dispose();
                Transaccion.Dispose();
                cn.Close();
                cn.Dispose();
                Parametros.Clear();
            }

            return logResp;

        }

        //Actualiza un registro seleccionado
        public bool ActualizarDatos(ref string Tabla, string NombrePK, int ValorPK, ref SqlParameterCollection Parametros, Int16 IDUSuario, string IP, ref bool RegistraBitacora)
        {
            bool logRespuesta = true;
            string CamposModificados;

            SqlConnection cn = new SqlConnection(mCN);
            cn.Open();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dtTabla = new DataTable();
            string Sql = "";
            string CamposSelect = "";
            string Cambios = "";
            int i = 0;

            for (i = 0; i <= Parametros.Count - 1; i++)
            {
                CamposSelect += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1);
                if (i < Parametros.Count - 1)
                {
                    CamposSelect += ",";
                }
            }
            Sql = "SELECT " + CamposSelect + " FROM " + Tabla + " WHERE " + NombrePK + "=" + ValorPK;

            da = new SqlDataAdapter(Sql, cn);
            da.Fill(dtTabla);

            cmd.Connection = cn;
            SqlTransaction Transaccion = cn.BeginTransaction();
            cmd.Transaction = Transaccion;
            cmd.CommandType = CommandType.Text;


            try
            {
                cmd.CommandText = "Update " + Tabla + " Set ";
                for (i = 0; i <= Parametros.Count - 1; i++)
                {
                    if (Information.IsDBNull(dtTabla.Rows[0].ItemArray[i]) || Information.IsDBNull(Parametros[i].Value))
                    {
                        if (dtTabla.Rows[0].ItemArray[i].ToString() != Parametros[i].Value.ToString())
                        {
                            Cambios += dtTabla.Columns[i].ColumnName + ";";

                            cmd.CommandText += dtTabla.Columns[i].ColumnName + "=" + Parametros[i].ParameterName + ",";
                            cmd.Parameters.Add(Parametros[i].ParameterName, Parametros[i].SqlDbType).Value = Parametros[i].Value;
                        }
                    }
                    else
                    {
                        if (dtTabla.Rows[0].ItemArray[i] != Parametros[i].Value)
                        {
                            Cambios += dtTabla.Columns[i].ColumnName + ";";

                            cmd.CommandText += dtTabla.Columns[i].ColumnName + "=" + Parametros[i].ParameterName + ",";
                            cmd.Parameters.Add(Parametros[i].ParameterName, Parametros[i].SqlDbType).Value = Parametros[i].Value;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Cambios))
                {
                    cmd.CommandText += " Where " + NombrePK + "=" + ValorPK;
                    cmd.CommandText = Strings.Replace(cmd.CommandText, ", Where ", " Where ");
                    cmd.ExecuteNonQuery();
                    if (RegistraBitacora)
                    {
                        cmd.CommandText = "Insert into M_BitacoraTransacciones (IDEmpresa,IDModulo,AccionBit,TablaBit,ValorLlavePrimaria,DescripcionBit,IDUsuario,Terminal) Values('C','" + Tabla + "'," + ValorPK + ",'" + Strings.Left(Cambios, 50) + "'," + IDUSuario + ",'" + IP + "')";
                        cmd.ExecuteNonQuery();
                    }
                }
                CamposModificados = Cambios;
                Transaccion.Commit();

                logRespuesta = true;
            }
            catch (Exception ex)
            {
                Transaccion.Rollback();
                LevantarError(ex);

                logRespuesta = false;
            }

            if (cmd != null)
            {
                cmd.Dispose();
                Transaccion.Dispose();
                cn.Close();
                cn.Dispose();
                Parametros.Clear();
            }

            return logRespuesta;
        }

        //Elimina un registro determinado de la tabla seleccionada.
        public bool EliminarDatos(ref string Tabla, string Campo, Int32 PK, Int16 IDUSuario, string IP, bool RegistraBitacora = true)
        {
            bool logRespuesta = true;

            SqlConnection cn = new SqlConnection(mCN);

            cn.Open();
            SqlCommand cmd = cn.CreateCommand();
            SqlTransaction Transaccion = cn.BeginTransaction();
            cmd.Transaction = Transaccion;
            cmd.CommandType = CommandType.Text;

            try
            {
                if (RegistraBitacora)
                {
                    //Insertar en la bitacora
                    cmd.CommandText = "Insert into M_BitacoraTransacciones (IDEmpresa,IDModulo,AccionBit,TablaBit,ValorLlavePrimaria,IDUsuario,Terminal) Values('B','" + Tabla + "'," + PK + "," + IDUSuario + ",'" + IP + "')";
                    cmd.ExecuteNonQuery();
                }
                //Construye query para realizar eliminacion
                cmd.CommandText = "Delete from " + Tabla + " Where " + Campo + "=" + PK;
                cmd.ExecuteNonQuery();
                Transaccion.Commit();

                logRespuesta = true;
            }
            catch (Exception ex)
            {
                Transaccion.Rollback();

                LevantarError(ex);
                logRespuesta = false;
            }

            if (cmd != null)
            {
                cmd.Dispose();
                Transaccion.Dispose();
                cn.Close();
                cn.Dispose();
            }

            return logRespuesta;
        }

        //Retorna un conjunto de datos de una tabla o vista especificada
        public DataTable RetornarDatos(string Tabla, string Campos,  SqlParameterCollection Parametros, string CondicionWhere)
        {
            DataTable dtResultado = new DataTable();
            SqlDataAdapter daResultado = default(SqlDataAdapter);

            try
            {
                SqlConnection cn = new SqlConnection(mCN);
                cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "SELECT " + Campos + "  FROM " + Tabla;

                if (Parametros != null)
                {
                    if (Parametros.Count > 0)
                    {
                        cmd.CommandText += " Where " + CondicionWhere;
                        for (int i = 0; i <= Parametros.Count - 1; i++)
                        {
                            cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                        }
                    }
                }

                daResultado = new SqlDataAdapter(cmd);
                daResultado.Fill(dtResultado);

                daResultado.Dispose();
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }
            return dtResultado;
        }

        //Retorna el valor maximo de un campo en una tabla, se utiliza con campos numericos
        public int Max(string Tabla, string Campo, SqlParameterCollection Parametros = null)
        {
            int Resultado = 0;
            try
            {
                SqlConnection cn = new SqlConnection(mCN);
                cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select isnull(max(" + Campo + "),0) + 1 From " + Tabla;

                if (Parametros != null)
                {
                    if (Parametros.Count > 0)
                    {
                        cmd.CommandText += " Where ";
                        for (int i = 0; i <= Parametros.Count - 1; i++)
                        {
                            cmd.CommandText += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1) + "=" + Parametros[i].ParameterName;
                            if (i < Parametros.Count - 1)
                            {
                                cmd.CommandText += " and ";
                            }
                            cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                        }
                    }
                }

                Resultado = Convert.ToInt32(cmd.ExecuteScalar());
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }
            return Resultado;
        }

        //Retorna el valor minimo de un campo en una tabla, se utiliza con campos numericos
        public int Min(string Tabla, string Campo, SqlParameterCollection Parametros = null)
        {
            int Resultado = 0;
            try
            {
                SqlConnection cn = new SqlConnection(mCN);
                cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select isnull(min(" + Campo + "),0)-1 From " + Tabla;

                if (Parametros != null)
                {
                    if (Parametros.Count > 0)
                    {
                        cmd.CommandText += " Where ";
                        for (int i = 0; i <= Parametros.Count - 1; i++)
                        {
                            cmd.CommandText += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1) + "=" + Parametros[i].ParameterName;
                            if (i < Parametros.Count - 1)
                            {
                                cmd.CommandText += " and ";
                            }
                            cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                        }
                    }
                }

                Resultado = Convert.ToInt32(cmd.ExecuteScalar());
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }
            return Resultado;
        }

        //Retorna el valor de un campo en una tabla, retorna cualquier campo a string
        public object RetornarValor(string Tabla, string Campo, SqlParameterCollection Parametros)
        {
            string Resultado = "";
            try
            {
                SqlConnection cn = new SqlConnection(mCN);
                cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select " + Campo + " From " + Tabla;

                cmd.CommandText += " Where ";
                for (int i = 0; i <= Parametros.Count - 1; i++)
                {
                    cmd.CommandText += Strings.Right(Parametros[i].ParameterName, Strings.Len(Parametros[i].ParameterName) - 1) + "=" + Parametros[i].ParameterName;
                    if (i < Parametros.Count - 1)
                    {
                        cmd.CommandText += " and ";
                    }
                    cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                }

                Resultado = Convert.ToString(cmd.ExecuteScalar());
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }
            return Resultado;
        }

        //Permite ejecutar un procedimiento almacenado, sin retorno de datos, opcionalmente retorna un parametro de salida. 
        public bool EjecutarSP(string NombreSP, ref SqlParameterCollection Parametros, bool ParametrosDeSalida = false)
        {
            bool logRepuesta = true;
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection(mCN);
            Int16 i = 0;
            cn.Open();

            try
            {
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = NombreSP;

                if (Parametros != null)
                {
                    for (i = 0; i <= Parametros.Count - 1; i++)
                    {
                        cmd.Parameters.Add(Parametros[i].ParameterName.ToString(), Parametros[i].SqlDbType, Parametros[i].Size).Value = Parametros[i].Value;
                        cmd.Parameters[i].Direction = Parametros[i].Direction;
                    }
                }

                cmd.ExecuteNonQuery();
                if (ParametrosDeSalida)
                {
                    for (i = 0; i <= cmd.Parameters.Count - 1; i++)
                    {
                        Parametros[i].Value = cmd.Parameters[i].Value;
                    }
                }

                cn.Close();
                cmd.Dispose();
                logRepuesta = true;
            }
            catch (Exception ex)
            {
                LevantarError(ex);

                logRepuesta = false;
            }

            return logRepuesta;
        }

        //permite ejecutar un procedimiento almacenado, retornando los datos en data table. Opcionalmente retorna parametros de salida.
        public DataTable EjecutarSPConDatos(string NombreSP, ref SqlParameterCollection Parametros, bool ParametrosDeSalida = false)
        {
            DataTable dtTabla = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlConnection cn = new SqlConnection(mCN);
            Int16 i = 0;
            cn.Open();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = NombreSP;
                if (Parametros != null)
                {
                    for (i = 0; i <= Parametros.Count - 1; i++)
                    {
                        cmd.Parameters.Add(Parametros[i].ParameterName.ToString(), Parametros[i].SqlDbType, Parametros[i].Size).Value = Parametros[i].Value;
                        cmd.Parameters[i].Direction = Parametros[i].Direction;
                    }
                }

                da = new SqlDataAdapter(cmd);
                da.Fill(dtTabla);

                if (ParametrosDeSalida)
                {
                    for (i = 0; i <= cmd.Parameters.Count - 1; i++)
                    {
                        Parametros[i].Value = cmd.Parameters[i].Value;
                    }
                }

            }
            catch (Exception ex)
            {
                LevantarError(ex);
            }

            cmd.Dispose();
            da.Dispose();

            return dtTabla;
        }

        //Ejecuta un comando de sql 
        public bool EjecutarSQL(ref string SQL, ref SqlParameterCollection Parametros)
        {
            bool logRespuesta = true;
            SqlConnection cn = new SqlConnection(mCN);
            SqlCommand cmd = new SqlCommand();
            cn.Open();
            cmd = new SqlCommand(SQL, cn);
            try
            {
                for (int i = 0; i <= Parametros.Count - 1; i++)
                {
                    cmd.Parameters.AddWithValue(Parametros[i].ParameterName, Parametros[i].Value);
                }
                cmd.ExecuteNonQuery();

                logRespuesta = true;
            }
            catch (Exception ex)
            {
                LevantarError(ex);
                logRespuesta = false;
            }

            if (cmd != null)
            {
                cmd.Dispose();
                cn.Close();
                cn.Dispose();
                Parametros.Clear();
            }

            return logRespuesta;
        }

        //funcion que invoca evento de error
        public void LevantarError(Exception ex)
        {
            MensajeError = ex.Message;

            OnErrorDelegado EventoError = onError;

            if (EventoError != null)
            {
                EventoError(MensajeError);
            }
            
        }
    }
}
